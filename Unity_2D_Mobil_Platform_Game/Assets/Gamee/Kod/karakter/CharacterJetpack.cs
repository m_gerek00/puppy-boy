﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Havada jetpack yapabilmesi için bu sınıfı bir karaktere ekleyin.
/// </summary>
public class CharacterJetpack : MonoBehaviour 
{

	/// karakterle ilişkili jetpack
	public ParticleSystem Jetpack;

	/// jetpack tarafından uygulanan kuvvet
	public float JetpackForce = 2.5f;
	/// karakterin jetpack için sınırsız yakıtı varsa doğrudur
	public bool JetpackUnlimited = false;
	/// jetpack'in maksimum süresi (saniye cinsinden)
	public float JetpackFuelDuration = 5f;
	/// jetpack cooldown
	public float JetpackRefuelCooldown=1f;
	
	private CharacterBehavior _characterBehavior;
	private CorgiController _controller;
	
	void Start () 
	{
		//özel değişkenleri başlat
		_characterBehavior = GetComponent<CharacterBehavior>();
		_controller = GetComponent<CorgiController>();
	
		if (Jetpack!=null)
		{
			Jetpack.enableEmission=false;
			GUIManager.Instance.SetJetpackBar (!JetpackUnlimited);
			_characterBehavior.BehaviorState.JetpackFuelDurationLeft = JetpackFuelDuration;		
		}
	}
	

	public void JetpackStart()
	{
		if ((!_characterBehavior.Permissions.JetpackEnabled)||(!_characterBehavior.BehaviorState.CanJetpack)||(_characterBehavior.BehaviorState.IsDead))
			return;
		
		// sıkıştığında jetpack açamaz
		if (!_characterBehavior.BehaviorState.CanMoveFreely)
			return;
		
		// yakıt bitince durur
		if ((!JetpackUnlimited) && (_characterBehavior.BehaviorState.JetpackFuelDurationLeft <= 0f)) 
		{
			// jetpack durdurma
			JetpackStop();
			_characterBehavior.BehaviorState.CanJetpack=false;
			return;
		}
		
		_controller.SetVerticalForce(JetpackForce);
		_characterBehavior.BehaviorState.Jetpacking=true;
		_characterBehavior.BehaviorState.CanMelee=false;
		_characterBehavior.BehaviorState.CanJump=false;
		Jetpack.enableEmission=true;
		// Jetpack Sınırlı olması durumu kontrolü
		if (!JetpackUnlimited) 
		{
			StartCoroutine (JetpackFuelBurn ());
			
		}
	}

	/// <summary>
	/// Karakterin jetpack'i durdurmasına neden olur.
	/// </summary>
	public void JetpackStop()
	{
		if (Jetpack==null)
			return;
		_characterBehavior.BehaviorState.Jetpacking=false;
		_characterBehavior.BehaviorState.CanMelee=true;
		Jetpack.enableEmission=false;
		_characterBehavior.BehaviorState.CanJump=true;
		// jetpack sınırsız değilse, yakıt ikmali yapmaya başlarız
		if (!JetpackUnlimited)
			StartCoroutine (JetpackRefuel());
	}


	/// <summary>
	/// Jetpack yakıtını yakar
	/// </summary>
	/// <returns>Yakıt bitiyor.</returns>
	private IEnumerator JetpackFuelBurn()
	{
		// karakter jetpack yaparken ve yakıtımız kalırken, kalan yakıtı azaltırız
		float timer =_characterBehavior.BehaviorState.JetpackFuelDurationLeft;
		while ((timer > 0) && (_characterBehavior.BehaviorState.Jetpacking))
		{
			timer -= Time.deltaTime;
			_characterBehavior.BehaviorState.JetpackFuelDurationLeft=timer;
			yield return 0;
		}
	}

	/// <summary>
	/// Jetpack yakıtını yeniden doldurur
	/// </summary>
	/// <returns>Yakıt doldurma.</returns>
	private IEnumerator JetpackRefuel()
	{
		// doldurmaya başlamadan önce bir süre bekleriz
		yield return new WaitForSeconds (JetpackRefuelCooldown);
		// daha sonra jetpack yakıtını kademeli olarak dolduruyoruz
		float timer =_characterBehavior.BehaviorState.JetpackFuelDurationLeft;
		while ((timer < JetpackFuelDuration) && (!_characterBehavior.BehaviorState.Jetpacking))
		{
			timer += Time.deltaTime/2;
			_characterBehavior.BehaviorState.JetpackFuelDurationLeft=timer;
			//Düşük yakıt ve yakıt ikmali sırasında karakterin tekrar jetpack yapmasını engelleriz
			if ((!_characterBehavior.BehaviorState.CanJetpack) && (timer > 1f))
				_characterBehavior.BehaviorState.CanJetpack=true;
			yield return 0;
		}
	}
	
	void Update () 
	{
	
	}
}
