using UnityEngine;
using System.Collections;

/// <summary>
/// Bu s�n�f� bir karaktere ekleyin, b�ylece mermi atabilir
/// </summary>
public class CharacterShoot : MonoBehaviour 
{
	/// karakterin sahip oldu�u ilk silah
	public Weapon InitialWeapon;

	/// silah�n tak�laca�� pozisyon
	public Transform WeaponAttachment;
	/// karakterin birden fazla y�nde �ekim yapmas�na izin verilirse do�rudur
	public bool EightDirectionShooting=true;
	/// karakter yaln�zca "kat�" 8 y�nde (�st, sa� �st, sa�, sa� alt vb.) ate� edebiliyorsa do�rudur
	public bool StrictEightDirectionShooting=true;	


	private Weapon _weapon;
	private float _fireTimer;

	private float _horizontalMove;
	private float _verticalMove;

	private CharacterBehavior _characterBehavior;
	private CorgiController _controller;

	void Start () 
	{
		// �zel de�i�kenleri ba�lat
		_characterBehavior = GetComponent<CharacterBehavior>();
		_controller = GetComponent<CorgiController>();

		// WeaponAttachment ayarlanmad�ysa dolgu
		if (WeaponAttachment==null)
			WeaponAttachment=transform;

		// ilk silah� kurduk
		ChangeWeapon(InitialWeapon);			
	}


	/// <summary>
	/// Karakteri bir kez vurur.
	/// </summary>
	public void ShootOnce()
	{
		// �zinlerde Vur eylemi etkinle�tirilmi�se devam ederiz, yoksa hi�bir �ey yapmazsak. Oyuncu �l�rse hi�bir �ey yapmay�z.
		if (!_characterBehavior.Permissions.ShootEnabled || _characterBehavior.BehaviorState.IsDead)
			return;
		// karakter ate� edemezse hi�bir �ey yapmay�z
		if (!_characterBehavior.BehaviorState.CanShoot)
		{
			// sadece ate�leme y�n�n� s�f�rlar�z (bu, �rne�in karakter bir merdivene ��kt���nda olur.
			_characterBehavior.BehaviorState.FiringDirection=3;
			return;		
		}

		// sadece ate�leme y�n�n� s�f�rlar�z (bu, �rne�in karakter bir merdivene ��kt���nda olur.
		if (!_characterBehavior.BehaviorState.CanMoveFreely)
			return;

		// bir mermi ate�liyoruz ve ate�leme zamanlay�c�s�n� s�f�rl�yoruz
		FireProjectile();	
		_fireTimer = 0;	
	}

	/// <summary>
	/// Karakterin �ekime ba�lamas�na neden olur
	/// </summary>
	public void ShootStart()
	{
		// �zinlerde Vur eylemi etkinle�tirilmi�se devam ederiz, yoksa hi�bir �ey yapmazsak. Oyuncu �l�rse hi�bir �ey yapmay�z.
		if (!_characterBehavior.Permissions.ShootEnabled || _characterBehavior.BehaviorState.IsDead)
			return;
		// karakter ate� edemezse hi�bir �ey yapmay�z
		if (!_characterBehavior.BehaviorState.CanShoot)
		{
			// sadece ate�leme y�n�n� s�f�rlar�z (bu, �rne�in karakter bir merdivene ��kt���nda olur.
			_characterBehavior.BehaviorState.FiringDirection=3;
			return;		
		}

		// karakter �zg�rce hareket edebilecek bir konumda de�ilse hi�bir �ey yapmay�z.
		if (!_characterBehavior.BehaviorState.CanMoveFreely)
			return;			
								
		_characterBehavior.BehaviorState.FiringStop = false;			
		_characterBehavior.BehaviorState.Firing = true;

		_weapon.SetGunFlamesEmission(true);
		_weapon.SetGunShellsEmission (true);
		_fireTimer += Time.deltaTime;
		if(_fireTimer > _weapon.FireRate)
		{
			FireProjectile();
			_fireTimer = 0; // ate� h�z� i�in zamanlay�c�y� s�f�rla
		}				
	}

	/// <summary>
	/// Karakterin �ekimi durdurmas�na neden olur
	/// </summary>
	public void ShootStop()
	{
		// �zinlerde Vur eylemi etkinle�tirilmi�se devam ederiz, yoksa hi�bir �ey yapmazsak
		if (!_characterBehavior.Permissions.ShootEnabled)
			return;
		// karakter ate� edemezse hi�bir �ey yapmay�z
		if (!_characterBehavior.BehaviorState.CanShoot)
		{
			// sadece ate�leme y�n�n� s�f�rlar�z (bu, �rne�in karakter bir merdivene ��kt���nda olur.
			_characterBehavior.BehaviorState.FiringDirection=3;
			return;		
		}
		_characterBehavior.BehaviorState.FiringStop = true;		
		_characterBehavior.BehaviorState.Firing = false;
		//ate�leme y�n�n� s�f�rla
		_characterBehavior.BehaviorState.FiringDirection=3;
		_weapon.GunFlames.enableEmission=false;
		_weapon.GunShells.enableEmission=false;	
	}

	/// <summary>
	/// Karakterin mevcut silah�n� parametre olarak ge�irilen silahla de�i�tirir
	/// </summary>
	/// <param name="newWeapon">The new weapon.</param>
	public void ChangeWeapon(Weapon newWeapon)
	{
		// karakterin zaten bir silah� varsa, onu durdururuz
		if (_weapon!=null)
		{
			ShootStop();
		}
		// silah �rne�i
		_weapon = (Weapon)Instantiate(newWeapon,WeaponAttachment.transform.position,WeaponAttachment.transform.rotation);	
		_weapon.transform.parent = transform;
		//silah yay�c�lar�n� kapat�yoruz.
		_weapon.SetGunFlamesEmission (false);
		_weapon.SetGunShellsEmission (false);
	}

	/// <summary>
	/// Mevcut silah�n mermilerinden birini ate�ler
	/// </summary>
	void FireProjectile () 
	{
		// oyuncunun girdi�i y�n� al�r�z.
		float HorizontalShoot = _horizontalMove;
		float VerticalShoot = _verticalMove;

		// mermi ate�leme yeri ayarlanmad�ysa, hi�bir �ey yapmay�z ve ��kmay�z
		if (_weapon.ProjectileFireLocation==null)
			return;

		// 8 y�nl� �ekim yapmak istemiyorsak bu iki �amand�ray� s�f�ra ayarl�yoruz.
		if (!EightDirectionShooting)
		{
			HorizontalShoot=0;
			VerticalShoot=0;
		}

		// 8 y�nl� kesin bir at�� istiyorsak, y�n de�erlerini yuvarl�yoruz.	
		if (StrictEightDirectionShooting)
		{
			HorizontalShoot = Mathf.Round(HorizontalShoot);
			VerticalShoot = Mathf.Round(VerticalShoot);
		}

		// a��y�, oyuncunun �ekim y�n�n� belirlemek i�in bast��� d��melere g�re hesaplar�z.					
		float angle = Mathf.Atan2(HorizontalShoot, VerticalShoot) * Mathf.Rad2Deg;
		
		Vector2 direction = Vector2.up;

		// kullan�c� herhangi bir y�n d��mesine basm�yorsa, �ekim y�n�n� bakt��� y�ne g�re belirleriz.
		if (HorizontalShoot>-0.1f && HorizontalShoot<0.1f && VerticalShoot>-0.1f && VerticalShoot<0.1f )
		{
			bool _isFacingRight = transform.localScale.x > 0;
			angle=_isFacingRight?90f : -90f;
			
		}

		// Animasyonu karakterin nerede �ekti�ine ba�l� olarak kurar�z

		// yukar� ate�
		if ( Mathf.Abs(HorizontalShoot)<0.1f && VerticalShoot>0.1f )
			_characterBehavior.BehaviorState.FiringDirection=1;
		// �apraz yukar� �ekim yap�l�yorsa
		if ( Mathf.Abs(HorizontalShoot)>0.1f && VerticalShoot>0.1f )
			_characterBehavior.BehaviorState.FiringDirection=2;
		// �apraz a�a��
		if ( Mathf.Abs(HorizontalShoot)>0.1f && VerticalShoot<-0.1f )
			_characterBehavior.BehaviorState.FiringDirection=4;
		// a�a�� ate�
		if ( Mathf.Abs(HorizontalShoot)<0.1f && VerticalShoot<-0.1f )
			_characterBehavior.BehaviorState.FiringDirection=5;
		if (Mathf.Abs(VerticalShoot)<0.1f)
			_characterBehavior.BehaviorState.FiringDirection=3;

		// Mermi Ate� Konumunu a��ya g�re hareket ettiriyoruz
		bool _facingRight = transform.localScale.x > 0;
		float horizontalDirection=_facingRight?1f:-1f;

		// a�� d�n���n� y�ne uygular�z.
		direction = Quaternion.Euler(0,0,-angle) * direction;

		// silah� y�ne do�ru d�nd�r�yoruz
		_weapon.GunRotationCenter.transform.rotation=Quaternion.Euler (new Vector3(0, 0, -angle*horizontalDirection+90f));

		// mermiyi mermiYang�nLokasyonu konumunda ba�lat�yoruz.		
		var projectile = (Projectile)Instantiate(_weapon.Projectile,_weapon.ProjectileFireLocation.position,_weapon.ProjectileFireLocation.rotation);
		projectile.Initialize(gameObject,direction,_controller.Speed);	
		
		// Ses
		if (_weapon.GunShootFx!=null)
			SoundManager.Instance.PlaySound(_weapon.GunShootFx,transform.position);		
	}
	
	private Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, float angle) 
	{
		
		angle = angle*(Mathf.PI/180f);
		var rotatedX = Mathf.Cos(angle) * (point.x - pivot.x) - Mathf.Sin(angle) * (point.y-pivot.y) + pivot.x;
		var rotatedY = Mathf.Sin(angle) * (point.x - pivot.x) + Mathf.Cos(angle) * (point.y - pivot.y) + pivot.y;
		return new Vector3(rotatedX,rotatedY,0);		
	}

	/// <summary>
	/// Yatay hareket de�erini ayarlar.
	/// </summary>
	/// <param name="value">Yatay hareket de�eri, -1 ile 1 aras�nda - pozitif: sa�a do�ru hareket eder, negatif: sola hareket eder </param>
	public void SetHorizontalMove(float value)
	{
		_horizontalMove=value;
	}

	/// <summary>
	/// Dikey hareket de�erini ayarlar.
	/// </summary>
	/// <param name="value">Dikey hareket de�eri, -1 ile 1 aras�nda
	public void SetVerticalMove(float value)
	{
		_verticalMove=value;
	}
	
	public void Flip()
	{
		if (_weapon.GunShells != null)
			_weapon.GunShells.transform.eulerAngles = new Vector3 (_weapon.GunShells.transform.eulerAngles.x, _weapon.GunShells.transform.eulerAngles.y + 180, _weapon.GunShells.transform.eulerAngles.z);
		if (_weapon.GunFlames != null)
			_weapon.GunFlames.transform.eulerAngles = new Vector3 (_weapon.GunFlames.transform.eulerAngles.x, _weapon.GunFlames.transform.eulerAngles.y + 180, _weapon.GunFlames.transform.eulerAngles.z);
	}
	
	void Update () 
	{
	
	}
}
