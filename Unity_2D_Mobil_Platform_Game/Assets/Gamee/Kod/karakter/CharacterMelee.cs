﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Yakın dövüş saldırıları yapabilmesi için bu sınıfı bir karaktere ekleyin
/// </summary>
public class CharacterMelee : MonoBehaviour 
{
	/// bir yakın dövüş çarpıştırıcısı2d (kutu, daire ...), tercihen karaktere iliştirilmiş.
	public GameObject MeleeCollider;
	/// Saldırının saniye cinsinden süresi
	public float MeleeAttackDuration=0.3f;

	private CharacterBehavior _characterBehavior;

	void Start () 
	{
		// özel değişkenleri başlat
		_characterBehavior = GetComponent<CharacterBehavior>();
		
		if (MeleeCollider!=null)
		{
			MeleeCollider.SetActive(false);
		}
	}

	/// <summary>
	/// Oyuncunun yakın muharebe saldırısını kullanarak saldırmasına neden olur
	/// </summary>
	public void Melee()
	{	
		// eğer bir şey yapmıyorsak,atak yapamaz
		if (!_characterBehavior.Permissions.MeleeAttackEnabled)
			return;
		// karakter ölüyse bişey yapamaz
		if (_characterBehavior.BehaviorState.IsDead)
			return;
		// karakter sıkıştığında atak yapamaz
		if (!_characterBehavior.BehaviorState.CanMoveFreely)
			return;
		
		// jetpack kullanırken atak yapamaz
		if (_characterBehavior.BehaviorState.CanMelee)
		{	
			// Yakın vuruş animasyonunu tetikliyor
			_characterBehavior.BehaviorState.MeleeAttacking=true;		
			MeleeCollider.SetActive(true);
			StartCoroutine(MeleeEnd());			
		}
	}
	
	
	private IEnumerator MeleeEnd()
	{
		// 0,3 delay
		yield return new WaitForSeconds(MeleeAttackDuration);
		MeleeCollider.SetActive(false);
		_characterBehavior.BehaviorState.MeleeAttacking=false;
	}
}
