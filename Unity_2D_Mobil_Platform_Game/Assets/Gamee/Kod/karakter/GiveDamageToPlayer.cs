using UnityEngine;
using System.Collections;
/// <summary>
/// Bu s�n�f�, onunla �arp���rken oyuncuya zarar vermesi gereken bir nesneye ekleyin
/// </summary>
public class GiveDamageToPlayer : MonoBehaviour 
{
	/// Oyuncunun sa�l���ndan kald�r�lacak sa�l�k miktar�
	public int DamageToGive = 10;
		
	private Vector2
		_lastPosition,
		_velocity;

	/// <summary>
	/// Son g�ncelleme s�ras�nda nesnenin konumunu ve h�z�n� kaydediyoruz
	/// </summary>
	public void LateUpdate () 
	{
		_velocity = (_lastPosition - (Vector2)transform.position) /Time.deltaTime;
		_lastPosition = transform.position;
	}

	/// <summary>
	/// Oyuncuyla bir �arp��ma tetiklendi�inde, oyuncuya hasar verir ve onu geri savururuz
	/// </summary>
	/// <param name="collider">nesne ile �arp��an nedir.</param>
	public void OnTriggerEnter2D(Collider2D collider)
	{
		var player=collider.GetComponent<CharacterBehavior>();
		if (player==null)
			return;
		
		player.TakeDamage(DamageToGive,gameObject);
		var controller=player.GetComponent<CorgiController>();
		var totalVelocity=controller.Speed + _velocity;
		
		controller.SetForce(new Vector2(
			-1*Mathf.Sign(totalVelocity.x) * Mathf.Clamp(Mathf.Abs(totalVelocity.x) * 5,10,20),
			-1*Mathf.Sign(totalVelocity.y) * Mathf.Clamp(Mathf.Abs(totalVelocity.y) * 2,0,15)			
			));
	}
}
