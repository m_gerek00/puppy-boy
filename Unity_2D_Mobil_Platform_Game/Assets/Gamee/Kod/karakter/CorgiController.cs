﻿using UnityEngine;
using System.Collections;
using UnitySampleAssets.CrossPlatformInput;
[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]

public class CorgiController : MonoBehaviour 
{
	public CorgiControllerState State { get; private set; }
	public CorgiControllerParameters2D DefaultParameters;
	public CorgiControllerParameters2D Parameters{get{return _overrideParameters ?? DefaultParameters;}}
	
	public LayerMask PlatformMask=0;
	public LayerMask MovingPlatformMask=0;
	public LayerMask EdgeColliderPlatformMask=0;
	public bool HandleCollisions { get; set; }
	public GameObject StandingOn { get; private set; }
	public Vector2 Speed { get{ return _speed; } }
	
	public int NumberOfHorizontalRays = 8;
	public int NumberOfVerticalRays = 8;
	
	public float RayOffset=0.05f; 
	private CorgiControllerParameters2D _overrideParameters;		
	private Vector2 _speed;
	private Vector2 _externalForce;
	private Vector2 _newPosition;
	private Transform _transform;
	private BoxCollider2D _boxCollider;
	private GameObject _lastStandingOn;
	
	private const float _largeValue=500000f;
	private const float _smallValue=0.0001f;

	private Rect _rayBoundsRectangle;
		
	/// <summary>
	/// Başlatma
	/// </summary>
	public void Awake()
	{
		HandleCollisions=true;
		_transform=transform;
		_boxCollider = (BoxCollider2D)GetComponent<BoxCollider2D>();
		State = new CorgiControllerState();
		
		// kenar çarpıştırıcı platformunu ve hareketli platform maskelerini ilk platform maskemize ekliyoruz.	
		PlatformMask |= EdgeColliderPlatformMask;
		PlatformMask |= MovingPlatformMask;
		
		State.Reset();
		SetRaysParameters();
	}
	
	/// <param name="force"> </param>
	public void AddForce(Vector2 force)
	{
		_speed += force;	
		_externalForce += force;
    }
    
    /// <param name="x"> </param>
    public void AddHorizontalForce(float x) // x ekseni ayarlamaları
    {
        _speed.x += x;
        _externalForce.x += x;
    }

    /// <param name="y"> </param>
    public void AddVerticalForce(float y) //y ekseni ayarlamaları
    {
        _speed.y += y;
        _externalForce.y += y;
    }
	
	/// <param name="force"> </param>
	public void SetForce(Vector2 force)
	{
		_speed = force;
		_externalForce = force;	
	}
	
	
	/// <param name="x"> </param>
	public void SetHorizontalForce (float x)
	{
		_speed.x = x;
		_externalForce.x = x;
	}
	
	
	/// <param name="y"> </param>
	public void SetVerticalForce (float y)
	{
		_speed.y = y;
		_externalForce.y = y;
	}
	
	private void Update()
	{
		
	}
	
	/// <summary>
	/// Her kareyi yerçekimine karakterinize uygularız, sonra bir nesneye çarpılıp çarpılmadığını kontrol eder ve yeni konumunu buna göre değiştiririz.
	///  Tüm kontroller yapıldığında, bu yeni pozisyonu uygularız.
	/// </summary>
	private void LateUpdate()
	{	
		_speed.y += Parameters.Gravity * Time.deltaTime;
		
		_newPosition=Speed * Time.deltaTime;
		SetRaysParameters();
		
		State.WasGroundedLastFrame = State.IsCollidingBelow;
        State.Reset(); 
		
		if (HandleCollisions)
		{
			CastRaysToTheSides();
			CastRaysBelow();		
			CastRaysAbove();			
		}
				
		_transform.Translate(_newPosition,Space.World);
		
		// hız
		if (Time.deltaTime > 0)
			_speed = _newPosition / Time.deltaTime;
		
		_externalForce.x=0;
		_externalForce.y=0;
		
		_speed.x = Mathf.Min (_speed.x,Parameters.MaxVelocity.x);
		_speed.y = Mathf.Min (_speed.y,Parameters.MaxVelocity.y);
		
		if( !State.WasGroundedLastFrame && State.IsCollidingBelow )
			State.JustGotGrounded=true;
	}
	
	
	private void CastRaysToTheSides() 
	{
		if (_newPosition.x == 0)
			return;
	
		float movementDirection=1;	
		if (_newPosition.x < 0)
			movementDirection = -1;
	
		float horizontalRayLength = Mathf.Abs(_speed.x*Time.deltaTime) + _rayBoundsRectangle.width/2 + RayOffset;
		
		Vector2 horizontalRayCastFromBottom=new Vector2(	_rayBoundsRectangle.center.x,
		                                          	_rayBoundsRectangle.yMin);										
		Vector2 horizontalRayCastToTop=new Vector2(	_rayBoundsRectangle.center.x,
		                                        	_rayBoundsRectangle.yMax);				
		
		RaycastHit2D[] hitsStorage = new RaycastHit2D[NumberOfHorizontalRays];
		float previousHitDistance=0; 		
			
		for (int i=0; i<NumberOfHorizontalRays;i++)
		{	
			Vector2 rayOriginPoint = Vector2.Lerp(horizontalRayCastFromBottom,horizontalRayCastToTop,(float)i/(float)(NumberOfHorizontalRays-1));
			
			if ( State.WasGroundedLastFrame && i == 0 )			
				hitsStorage[i] = CorgiRayCast (rayOriginPoint,movementDirection*Vector2.right,horizontalRayLength,PlatformMask,true,Color.red);	
			else
				hitsStorage[i] = CorgiRayCast (rayOriginPoint,movementDirection*Vector2.right,horizontalRayLength,PlatformMask & ~EdgeColliderPlatformMask,true,Color.red);			
			
			if (hitsStorage[i].distance >0)
			{				
				if (previousHitDistance>0)
				{					
					float hitAngle = Mathf.Abs(Vector2.Angle(hitsStorage[i].point - hitsStorage[i-1].point, Vector2.right)-90);
					if (hitAngle < 45f)
					{						
						if (movementDirection < 0)		
							State.IsCollidingLeft=true;
						else
							State.IsCollidingRight=true;						
						
						State.SlopeAngleOK=false;
						_newPosition.x=0;
						_speed = new Vector2(0, _speed.y);
						break;
					}					
				}
				previousHitDistance = hitsStorage[i].distance;
			}				
		}	
	}
	
	private void CastRaysBelow()
    {
		if (_newPosition.y < -_smallValue)
		{
			State.IsFalling=true;
		}
        else
        {
            State.IsFalling = false;
        }
        						
		if ((!State.IsFalling)&&(!State.IsGrounded))
			return;
        
		float rayLength = Mathf.Abs(_newPosition.y) +_rayBoundsRectangle.height/2 + RayOffset ; 		
		Vector2 verticalRayCastFromLeft=new Vector2(_rayBoundsRectangle.xMin+_newPosition.x,
		                                        	_rayBoundsRectangle.center.y+RayOffset);	
		Vector2 verticalRayCastToRight=new Vector2(	_rayBoundsRectangle.xMax+_newPosition.x,
		                                           _rayBoundsRectangle.center.y+RayOffset);					
	
		RaycastHit2D[] hitsStorage = new RaycastHit2D[NumberOfVerticalRays];
		float smallestDistance=_largeValue; 
		int smallestDistanceIndex=0; 						
		bool hitConnected=false; 		
		
		for (int i=0; i<NumberOfVerticalRays;i++)
		{			
			Vector2 rayOriginPoint = Vector2.Lerp(verticalRayCastFromLeft,verticalRayCastToRight,(float)i/(float)(NumberOfVerticalRays-1));
			
			if ((_newPosition.y>0) && (!State.WasGroundedLastFrame))
				hitsStorage[i] = CorgiRayCast (rayOriginPoint,-Vector2.up,rayLength,PlatformMask & ~EdgeColliderPlatformMask,true,Color.blue);	
			else
				hitsStorage[i] = CorgiRayCast (rayOriginPoint,-Vector2.up,rayLength,PlatformMask,true,Color.blue);					
						
			if ((Mathf.Abs(hitsStorage[smallestDistanceIndex].point.y - verticalRayCastFromLeft.y)) <  _smallValue)
			{
				break;
			}		
									
			if (hitsStorage[i])
			{
				hitConnected=true;
				if (hitsStorage[i].distance<smallestDistance)
				{
					smallestDistanceIndex=i;
					smallestDistance = hitsStorage[i].distance;
				}
			}								
		}
		if (hitConnected)
		{

			State.IsFalling=false;			
			State.IsCollidingBelow=true;
					
			_newPosition.y = -Mathf.Abs(hitsStorage[smallestDistanceIndex].point.y - verticalRayCastFromLeft.y) 
								+ _rayBoundsRectangle.height/2 
								+ RayOffset;
            if (_externalForce.y>0)
			{
				_newPosition.y += _speed.y * Time.deltaTime;
                State.IsCollidingBelow = false;
            }

            if (!State.WasGroundedLastFrame && _speed.y>0)
            {
                _newPosition.y += _speed.y * Time.deltaTime;
            }

			
			if (Mathf.Abs(_newPosition.y)<_smallValue)
				_newPosition.y = 0;
			
			// karakterin hareketli bir platformda durup durmadığını kontrol ediyoruz
			StandingOn=hitsStorage[smallestDistanceIndex].collider.gameObject;
			PathFollow movingPlatform = hitsStorage[smallestDistanceIndex].collider.GetComponent<PathFollow>();
			if (movingPlatform!=null)
				_transform.Translate(movingPlatform.CurrentSpeed*Time.deltaTime);
		}
		else
		{
			State.IsCollidingBelow=false;
		}		
	}
	
	private void CastRaysAbove()
	{
		
		if (_newPosition.y>0 || State.IsGrounded)
		{
			float rayLength = State.IsGrounded?RayOffset : _newPosition.y*Time.deltaTime;
			rayLength+=_rayBoundsRectangle.height/2;
			
			bool hitConnected=false; 
			int hitConnectedIndex=0; 
			
			Vector2 verticalRayCastStart=new Vector2(_rayBoundsRectangle.xMin,
			                                            _rayBoundsRectangle.center.y);	
			Vector2 verticalRayCastEnd=new Vector2(	_rayBoundsRectangle.xMax,
			                                           _rayBoundsRectangle.center.y);	
			                                           
			RaycastHit2D[] hitsStorage = new RaycastHit2D[NumberOfVerticalRays];
			
			for (int i=0; i<NumberOfVerticalRays;i++)
			{							
				Vector2 rayOriginPoint = Vector2.Lerp(verticalRayCastStart,verticalRayCastEnd,(float)i/(float)(NumberOfVerticalRays-1));
				hitsStorage[i] = CorgiRayCast (rayOriginPoint,Vector2.up,rayLength,PlatformMask & ~EdgeColliderPlatformMask,true,Color.green);	
				
				if (hitsStorage[i])
				{
					hitConnected=true;
					hitConnectedIndex=i;
					break;
				}
			}	
			
			if (hitConnected)
			{
				_speed.y=0;
				_newPosition.y = hitsStorage[hitConnectedIndex].distance - _rayBoundsRectangle.height/2   ;
				State.IsCollidingAbove=true;
			}				
		}
	}
	
	public void SetRaysParameters() 
	{		
		_rayBoundsRectangle = new Rect(_boxCollider.bounds.min.x,
		                               _boxCollider.bounds.min.y,
		                               _boxCollider.bounds.size.x,
		                               _boxCollider.bounds.size.y);	
		
		Debug.DrawLine(new Vector2(_rayBoundsRectangle.center.x,_rayBoundsRectangle.yMin),new Vector2(_rayBoundsRectangle.center.x,_rayBoundsRectangle.yMax));  
		Debug.DrawLine(new Vector2(_rayBoundsRectangle.xMin,_rayBoundsRectangle.center.y),new Vector2(_rayBoundsRectangle.xMax,_rayBoundsRectangle.center.y));
		
	}
	
	
	/// <returns> </returns>
	/// <param name="rayOriginPoint"> </param>
	/// <param name="rayDirection"> </param>
	/// <param name="rayDistance"> </param>
	/// <param name="mask"> </param>
	/// <param name="debug">If set to <c>true</c> debug.</param>
	/// <param name="color"> </param>
	private RaycastHit2D CorgiRayCast(Vector2 rayOriginPoint, Vector2 rayDirection, float rayDistance, LayerMask mask,bool debug,Color color)
	{			
		Debug.DrawRay( rayOriginPoint, rayDirection*rayDistance, color );
		return Physics2D.Raycast(rayOriginPoint,rayDirection,rayDistance,mask);		
	}
	
	
	/// <param name="duration"> </param>
	public IEnumerator DisableCollisions(float duration)
	{
		HandleCollisions = false;
		yield return new WaitForSeconds (duration);
		HandleCollisions = true;
	}
	
	/// <param name="collider"> </param>
	public void OnTriggerEnter2D(Collider2D collider)
	{
		CorgiControllerPhysicsVolume2D parameters = collider.gameObject.GetComponent<CorgiControllerPhysicsVolume2D>();
		if (parameters == null)
			return;
		_overrideParameters = parameters.ControllerParameters;
	}	
	
	/// <param name="collider"> </param>
	public void OnTriggerStay2D( Collider2D collider )
	{
	}	
	
	/// <param name="collider"> </param>
	public void OnTriggerExit2D(Collider2D collider)
	{		
		CorgiControllerPhysicsVolume2D parameters = collider.gameObject.GetComponent<CorgiControllerPhysicsVolume2D>();
		if (parameters == null)
			return;
		
		_overrideParameters = null;
	}
	
	
}
