﻿using System;
using UnityEngine;
using System.Collections;

[Serializable]
public class CharacterBehaviorParameters 
{
	/// karakterin ne kadar yükseğe zıplayabileceğini tanımlar
	public float JumpHeight = 3.025f;
	/// zıplarken havada izin verilen minimum süre - bu, basınç kontrollü sıçramalar için kullanılır
	public float JumpMinimumAirTime = 0.1f;
	/// temel hareket hızı
	public float MovementSpeed = 8f;
	/// karakterin çömelirkenki hızı
	public float CrouchSpeed = 4f;
	/// karakterin yürürken hızı
	public float WalkSpeed = 8f;
	/// karakterin koşarkenki hızı
	public float RunSpeed = 16f;
	/// bir merdivene tırmanırken karakterin hızı
	public float LadderSpeed = 2f;
	/// tire süresi (saniye cinsinden)
	public float DashDuration = 0.15f;
	/// çizginin gücü
	public float DashForce = 5f;
	/// 2 çizgi arasındaki soğuma süresi (saniye cinsinden)
	public float DashCooldown = 2f;
	/// duvar sıçramasının süresi (saniye cinsinden)
	public float WallJumpDuration = 0.15f;
	/// duvar sıçramasının gücü
	public float WallJumpForce = 3f;
	/// izin verilen maksimum atlama sayısı (0: atlama yok, 1: normal atlama, 2: çift atlama, vb.)
	public int NumberOfJumps=3;
	/// karakterin maksimum sağlığı
	public int MaxHealth = 100;
	public enum JumpBehavior
	{
		CanJumpOnGround,
		CanJumpAnywhere,
		CantJump,
		CanJumpAnywhereAnyNumberOfTimes
	}
	/// atlamalar için temel kurallar: oyuncu nereye atlayabilir?
	public JumpBehavior JumpRestrictions;
	/// doğruysa, atlama süresi / yüksekliği düğmeye basma süresiyle orantılı olacaktır
	public bool JumpIsProportionalToThePressTime=true;
}
