﻿using System;
using UnityEngine;
using System.Collections;

/// <summary>
/// Corgi Controller sınıfı için parametreler.
/// Eğim sınırınızı, yerçekimini ve hız sönümleme faktörlerinizi burada tanımlayabilirsiniz.
/// </summary>

[Serializable]
public class CorgiControllerParameters2D 
{
	/// Örneğin bir yokuşta çok hızlı hareket etmesini önlemek için karakteriniz için maksimum hız
	public Vector2 MaxVelocity = new Vector2(200f, 200f);
	public int MaxSpeed=200;
	/// Karakterin üzerinde yürüyebileceği maksimum açı (derece cinsinden)
	[Range(0,90)]
	public float MaximumSlopeAngle = 45;		
	/// Gravity
	public float Gravity = -15;
	// Yerdeki hız faktörü
	public float SpeedAccelerationOnGround = 20f;
	// Havadaki hız faktörü
	public float SpeedAccelerationInAir = 5f;	
}
