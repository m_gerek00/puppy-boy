﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Hasar alabilecek nesneler için genel arayüz
/// </summary>

public interface CanTakeDamage
{

	void TakeDamage(int damage,GameObject instigator);
}
