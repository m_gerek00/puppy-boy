﻿using System;
using UnityEngine;
using System.Collections;


[Serializable]
public class CharacterBehaviorPermissions
{

	// Oyuncunun ASLA atlamasını, atılmasını veya aşağıdaki eylemlerden herhangi birini yapmasını istemiyorsanız bunları yanlış olarak ayarlayın.
	// (bunları başlangıçta ayarlayabilir veya oyununuzda kilidini açabilirsiniz)	
	public bool RunEnabled=true;
	public bool DashEnabled=true;
	public bool JetpackEnabled=true;
	public bool JumpEnabled=true;
	public bool CrouchEnabled=true;
	public bool ShootEnabled=true;
	public bool WallJumpEnabled=true;
	public bool WallClingingEnabled=true;
	public bool MeleeAttackEnabled=true;	
}
