/// <summary>
/// Oyuncunun yeniden do�mas� i�in aray�z
/// </summary>
public interface IPlayerRespawnListener
{
	void onPlayerRespawnInThisCheckpoint(CheckPoint checkpoint, CharacterBehavior player);
}