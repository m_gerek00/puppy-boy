﻿using UnityEngine;
using System.Collections;
using UnitySampleAssets.CrossPlatformInput;

public class CharacterBehavior : MonoBehaviour,CanTakeDamage
{
	/// karakterin kafasının herhangi bir şeye çarpıp çarpmadığını kontrol etmek için kullanılacak boxcollider2D (çoğunlukla çömeldiğinde kullanılır)
	public BoxCollider2D HeadCollider ;
	/// karakter yere her dokunduğunda ortaya çıkacak etki
	public ParticleSystem TouchTheGroundEffect;
	/// karakter yere her dokunduğunda ortaya çıkacak etki
	public ParticleSystem HurtEffect;
	/// karakterin mevcut sağlığı
	public int Health {get; private set; }	
	public CharacterBehaviorState BehaviorState { get; private set; }

	public CharacterBehaviorParameters DefaultBehaviorParameters;	

	public CharacterBehaviorParameters BehaviorParameters{get{return _overrideBehaviorParameters ?? DefaultBehaviorParameters;}}

	public CharacterBehaviorPermissions Permissions ;

	public AudioClip PlayerJumpSfx;

	public AudioClip PlayerHitSfx;
	
	public bool JumpAuthorized 
	{ 
		get 
		{ 
			if ( (BehaviorParameters.JumpRestrictions == CharacterBehaviorParameters.JumpBehavior.CanJumpAnywhere) ||  (BehaviorParameters.JumpRestrictions == CharacterBehaviorParameters.JumpBehavior.CanJumpAnywhereAnyNumberOfTimes) )
				return true;
			
			if (BehaviorParameters.JumpRestrictions == CharacterBehaviorParameters.JumpBehavior.CanJumpOnGround)
				return _controller.State.IsGrounded;
			
			return false; 
		}
	}
	
	private GameObject _sceneCamera;
	private CorgiController _controller;

	private Animator _animator;
	private CharacterJetpack _jetpack;
	private CharacterShoot _shoot;

	// davranış parametrelerini geçersiz kılmak için depolama
	private CharacterBehaviorParameters _overrideBehaviorParameters;
	// orijinal yerçekimi ve zamanlayıcı için depolama
	private float _originalGravity;

	// mevcut normalleştirilmiş yatay hız
	private float _normalizedHorizontalSpeed;

	// basınç zamanlı atlamalar
	private float _jumpButtonPressTime = 0;
	private bool _jumpButtonPressed=false;
	private bool _jumpButtonReleased=false;
	
	private bool _isFacingRight=true;
	
	private float _horizontalMove;
	private float _verticalMove;
	
	
	void Awake()
	{		
		BehaviorState = new CharacterBehaviorState();
		_sceneCamera = GameObject.FindGameObjectWithTag("MainCamera");
		_controller = GetComponent<CorgiController>();
		_jetpack = GetComponent<CharacterJetpack>();
		_shoot = GetComponent<CharacterShoot> ();
		Health=BehaviorParameters.MaxHealth;
	}
	
	public void Start()
	{
		_animator = GetComponent<Animator>();
		_isFacingRight = transform.localScale.x > 0;
		
		_originalGravity = _controller.Parameters.Gravity;
		
		BehaviorState.Initialize();
		BehaviorState.NumberOfJumpsLeft=BehaviorParameters.NumberOfJumps;
		

		BehaviorState.CanJump=true;		
	}
	
	
	void Update()
	{			
		UpdateAnimator ();

		if (!BehaviorState.IsDead)
		{
			GravityActive(true);			
			HorizontalMovement();
			VerticalMovement();			
			
			BehaviorState.CanShoot=true;

			// merdiven tırmanma ve duvara tutunma
			ClimbLadder();
			WallClinging ();
	
			if (BehaviorState.Dashing) 
			{
				GravityActive(false);
				_controller.SetVerticalForce(0);
			}
			// karakter ateşlenmiyorsa, ateşleme Durdurma durumunu sıfırlarız.
			if (!BehaviorState.Firing)
			{
				BehaviorState.FiringStop=false;
			}
			// karakter zıplayabiliyorsa, zaman kontrollü zıplamaları hallederiz
			if (JumpAuthorized)
			{
				// Kullanıcı atlama düğmesini bırakırsa ve karakter zıplıyorsa ve ilk atlama geçtikten sonra yeterince zaman geçerse, o zaman bir kuvvet uygulayarak zıplamasını durdururuz.
				if ( (_jumpButtonPressTime!=0) 
				    && (Time.time - _jumpButtonPressTime >= BehaviorParameters.JumpMinimumAirTime) 
				    && (_controller.Speed.y > Mathf.Sqrt(Mathf.Abs(_controller.Parameters.Gravity))) 
				    && (_jumpButtonReleased)
				    && (!_jumpButtonPressed||BehaviorState.Jetpacking))
				{
					_jumpButtonReleased=false;	
					if (BehaviorParameters.JumpIsProportionalToThePressTime)					
						_controller.AddForce(new Vector2(0,12 * -Mathf.Abs(_controller.Parameters.Gravity) * Time.deltaTime ));			
				}
			}			
		}
		else
		{
			// karakter ölüyse yatay hareket etmesini engelleriz
			_controller.SetHorizontalForce(0);
		}
	}
	
	void LateUpdate()
	{
		// karakter bu karede topraklanırsa, doubleJump bayrağını sıfırlarız, böylece tekrar iki katına atlayabilir
		if (_controller.State.JustGotGrounded)
		{
			BehaviorState.NumberOfJumpsLeft=BehaviorParameters.NumberOfJumps;		
		}
		
	}
	
	private void UpdateAnimator()
	{	
		_animator.SetBool("Grounded",_controller.State.IsGrounded);
		_animator.SetFloat("Speed",Mathf.Abs(_controller.Speed.x));
		_animator.SetFloat("vSpeed",_controller.Speed.y);
		_animator.SetBool("Running",BehaviorState.Running);
		_animator.SetBool("Dashing",BehaviorState.Dashing);
		_animator.SetBool("Crouching",BehaviorState.Crouching);
		_animator.SetBool("LookingUp",BehaviorState.LookingUp);
		_animator.SetBool("WallClinging",BehaviorState.WallClinging);
		_animator.SetBool("Jetpacking",BehaviorState.Jetpacking);
		_animator.SetBool("Diving",BehaviorState.Diving);
		_animator.SetBool("LadderClimbing",BehaviorState.LadderClimbing);
		_animator.SetFloat("LadderClimbingSpeed",BehaviorState.LadderClimbingSpeed);
		_animator.SetBool("FiringStop",BehaviorState.FiringStop);
		_animator.SetBool("Firing",BehaviorState.Firing);
		_animator.SetInteger("FiringDirection",BehaviorState.FiringDirection);
		_animator.SetBool("MeleeAttacking",BehaviorState.MeleeAttacking);
	}

	/// <param name="value">Yatay hareket değeri, -1 ile 1 arasında - pozitif: sağa doğru hareket eder, negatif: sola hareket eder </param>
	public void SetHorizontalMove(float value)
	{
		_horizontalMove=value;
	}

	/// <param name="value">Dikey hareket değeri, -1 ile 1 arasında
	public void SetVerticalMove(float value)
	{
		_verticalMove=value;
	}
	
	private void HorizontalMovement()
	{
		// hareket engellenirse çıkarız ve hiçbir şey yapmayız
		if (!BehaviorState.CanMoveFreely)
			return;

		// Yatay eksenin değeri pozitifse, karakter sağa bakmalıdır.
		if (_horizontalMove>0.1)
		{
			_normalizedHorizontalSpeed = _horizontalMove;
			if (!_isFacingRight)
				Flip();
		}		
		else if (_horizontalMove<-0.1)
		{
			_normalizedHorizontalSpeed = _horizontalMove;
			if (_isFacingRight)
				Flip();
		}
		else
		{
			_normalizedHorizontalSpeed=0;
		}

		// Denetleyiciye uygulanması gereken yatay kuvveti iletiyoruz.
		var movementFactor = _controller.State.IsGrounded ? _controller.Parameters.SpeedAccelerationOnGround : _controller.Parameters.SpeedAccelerationInAir;
		_controller.SetHorizontalForce(Mathf.Lerp(_controller.Speed.x, _normalizedHorizontalSpeed * BehaviorParameters.MovementSpeed, Time.deltaTime * movementFactor));
		
	}
	
	private void VerticalMovement()
	{

		if (_controller.State.JustGotGrounded)
		{
			Instantiate(TouchTheGroundEffect,new Vector2(transform.position.x,transform.position.y-transform.localScale.y/2),transform.rotation);	
		}
		
		if (!BehaviorState.CanMoveFreely)
			return;
		
		if ( (_verticalMove<-0.1) && (_controller.State.IsGrounded) && (Permissions.CrouchEnabled) )
		{
			BehaviorState.Crouching = true;
			BehaviorParameters.MovementSpeed = BehaviorParameters.CrouchSpeed;
			BehaviorState.Running=false;
			
		}
		else
		{	
		
			if (BehaviorState.Crouching)
			{	
				if (HeadCollider==null)
					return;
				bool headCheck = Physics2D.OverlapCircle(HeadCollider.transform.position,HeadCollider.size.x/2,_controller.PlatformMask);			

				if (!headCheck)
				{
					if (!BehaviorState.Running)
						BehaviorParameters.MovementSpeed = BehaviorParameters.WalkSpeed;
					BehaviorState.Crouching = false;
					BehaviorState.CanJump=true;
				}
				else
				{
					
					BehaviorState.CanJump=false;
				}
			}
		}
		
		if (BehaviorState.CrouchingPreviously!=BehaviorState.Crouching)
		{
			Invoke ("RecalculateRays",Time.deltaTime*10);		
		}
		
		BehaviorState.CrouchingPreviously=BehaviorState.Crouching;
		
		if ( (_verticalMove>0) && (_controller.State.IsGrounded) )
		{
			BehaviorState.LookingUp = true;		
		}
		else
		{
			BehaviorState.LookingUp = false;
		}
		
	}
	
	public void RecalculateRays()
	{
		_controller.SetRaysParameters();
	}
	
	public void RunStart()
	{		
		if (!Permissions.RunEnabled)
			return;
		if (!BehaviorState.CanMoveFreely)
			return;
		
		if (_controller.State.IsGrounded && !BehaviorState.Crouching)
		{
			BehaviorParameters.MovementSpeed = BehaviorParameters.RunSpeed;
			BehaviorState.Running=true;
		}
	}
	
	public void RunStop()
	{
		BehaviorParameters.MovementSpeed = BehaviorParameters.WalkSpeed;
		BehaviorState.Running=false;
	}
	
	public void JumpStart()
	{
		
		if (!Permissions.JumpEnabled  || !JumpAuthorized || BehaviorState.IsDead)
			return;
		
		if (_controller.State.IsGrounded 
		    || BehaviorState.LadderClimbing 
		    || BehaviorState.WallClinging 
		    || BehaviorState.NumberOfJumpsLeft>0) 	
			BehaviorState.CanJump=true;
		else
			BehaviorState.CanJump=false;
					
		if ( (!BehaviorState.CanJump) && !(BehaviorParameters.JumpRestrictions==CharacterBehaviorParameters.JumpBehavior.CanJumpAnywhereAnyNumberOfTimes) )
			return;
		
		if (_verticalMove<0 && _controller.State.IsGrounded)
		{
			if (_controller.StandingOn.layer==11)
			{
				_controller.transform.position=new Vector2(transform.position.x,transform.position.y-0.1f);
				StartCoroutine(_controller.DisableCollisions(0.3f));
				return;
			}
		}
		
		BehaviorState.NumberOfJumpsLeft=BehaviorState.NumberOfJumpsLeft-1;
		
		BehaviorState.LadderClimbing=false;
		BehaviorState.CanMoveFreely=true;
		GravityActive(true);
		
		_jumpButtonPressTime=Time.time;
		_jumpButtonPressed=true;
		_jumpButtonReleased=false;
				
		_controller.SetVerticalForce(Mathf.Sqrt( 2f * BehaviorParameters.JumpHeight * Mathf.Abs(_controller.Parameters.Gravity) ));
		
		if (PlayerJumpSfx!=null)
			SoundManager.Instance.PlaySound(PlayerJumpSfx,transform.position);
		
		float wallJumpDirection;
		if (BehaviorState.WallClinging)
		{
			
			if (_controller.State.IsCollidingRight)
			{
				wallJumpDirection=-1f;
			}
			else
			{					
				wallJumpDirection=1f;
			}
			StartCoroutine( Boost(BehaviorParameters.WallJumpDuration,wallJumpDirection*BehaviorParameters.WallJumpForce,0,"wallJump") );
			BehaviorState.WallClinging=false;
		}	
		
	}
	
	public void JumpStop()
	{
		_jumpButtonPressed=false;
		_jumpButtonReleased=true;
	}
	
	
	void ClimbLadder()
	{
		if (BehaviorState.LadderColliding)
		{
			if (_verticalMove>0.1 && !BehaviorState.LadderClimbing && !BehaviorState.LadderTopColliding  && !BehaviorState.Jetpacking)
			{			
				BehaviorState.LadderClimbing=true;
				BehaviorState.CanMoveFreely=false;
				if (_shoot!=null)
					_shoot.ShootStop();
				BehaviorState.LadderClimbingSpeed=0;
				_controller.SetHorizontalForce(0);
				_controller.SetVerticalForce(0);
				GravityActive(false);
			}			
			
			if (BehaviorState.LadderClimbing)
			{
				BehaviorState.CanShoot=false;
				GravityActive(false);
				_controller.SetVerticalForce(_verticalMove * BehaviorParameters.LadderSpeed);
				BehaviorState.LadderClimbingSpeed=Mathf.Abs(_verticalMove);				
			}
			
			if (BehaviorState.LadderClimbing && _controller.State.IsGrounded)
			{
				BehaviorState.LadderColliding=false;
				BehaviorState.LadderClimbing=false;
				BehaviorState.CanMoveFreely=true;
				BehaviorState.LadderClimbingSpeed=0;	
				GravityActive(true);			
			}			
		}
		
		if (BehaviorState.LadderTopColliding && _verticalMove<-0.1 && !BehaviorState.LadderClimbing && _controller.State.IsGrounded)
		{
			BehaviorState.LadderEnteredViaTop=true;
			_controller.HandleCollisions=false;
			transform.position=new Vector2(transform.position.x,transform.position.y-0.1f);
			BehaviorState.LadderClimbing=true;
			BehaviorState.CanMoveFreely=false;
			BehaviorState.LadderClimbingSpeed=0;			
			_controller.SetHorizontalForce(0);
			_controller.SetVerticalForce(0);
			GravityActive(false);
		}
		if ((!BehaviorState.LadderTopColliding)&&(BehaviorState.LadderEnteredViaTop))
		{			
			_controller.HandleCollisions=true;
			BehaviorState.LadderEnteredViaTop=false;
		}
		
	}
	
	public void Dash()
	{	
		
		float _dashDirection;
		float _boostForce;
				
		if (!Permissions.DashEnabled || BehaviorState.IsDead)
			return;
		if (!BehaviorState.CanMoveFreely)
			return;
		
		if (_verticalMove>-0.8) 
		{	
			if (BehaviorState.CanDash)
			{
				BehaviorState.Dashing=true;
				
				if (_isFacingRight) { _dashDirection=1f; } else { _dashDirection = -1f; }
				_boostForce=_dashDirection*BehaviorParameters.DashForce;
				BehaviorState.CanDash = false;
				StartCoroutine( Boost(BehaviorParameters.DashDuration,_boostForce,0,"dash") );
			}			
		}
		if (_verticalMove<-0.8) 
		{
			StartCoroutine(Dive());
		}		
		
	}

	IEnumerator Boost(float boostDuration, float boostForceX, float boostForceY, string name) 
	{
		float time = 0f; 
		
		while(boostDuration > time) 
		{
			if (boostForceX!=0)
			{
				_controller.AddForce(new Vector2(boostForceX,0));
			}
			if (boostForceY!=0)
			{
				_controller.AddForce(new Vector2(0,boostForceY));
			}
			time+=Time.deltaTime;
			yield return 0; 
		}
		if (name=="dash")
		{
			BehaviorState.Dashing=false;
			GravityActive(true);
			yield return new WaitForSeconds(BehaviorParameters.DashCooldown); 
			BehaviorState.CanDash = true; 
		}	
		if (name=="wallJump")
		{
		}		
	}

	IEnumerator Dive()
	{	
		Vector3 ShakeParameters = new Vector3(1.5f,0.5f,1f);
		BehaviorState.Diving=true;
		while (!_controller.State.IsGrounded)
		{
			_controller.SetVerticalForce(-Mathf.Abs(_controller.Parameters.Gravity)*2);
			yield return 0; 
		}
		
		_sceneCamera.SendMessage("Shake",ShakeParameters);		
		BehaviorState.Diving=false;
	}
	
	
	private void WallClinging()
	{
		if (!Permissions.WallClingingEnabled)
			return;
		if (!_controller.State.IsCollidingLeft && !_controller.State.IsCollidingRight)
		{
			BehaviorState.WallClinging=false;
		}
		
		if (!BehaviorState.CanMoveFreely)
			return;
		
		if((!_controller.State.IsGrounded) && ( ( (_controller.State.IsCollidingRight) && (_horizontalMove>-0.1f) )	|| 	( (_controller.State.IsCollidingLeft) && (_horizontalMove<0.1f) )	))
		{
			if (_controller.Speed.y<0)
			{
				BehaviorState.WallClinging=true;
				_controller.AddForce(new Vector2(0,(Mathf.Abs(_controller.Parameters.Gravity) * Time.deltaTime)*0.9f));
			}
		}
		else
		{
			BehaviorState.WallClinging=false;
		}
	}

	/// <param name="state"><c> true </c> olarak ayarlanırsa, yerçekimini etkinleştirir. <c> false </c> olarak ayarlanırsa, kapatın.</param>
	private void GravityActive(bool state)
	{
		if (state==true)
		{
			if (_controller.Parameters.Gravity==0)
			{
				_controller.Parameters.Gravity = _originalGravity;
			}
		}
		else
		{
			if (_controller.Parameters.Gravity!=0)
				_originalGravity = _controller.Parameters.Gravity;
			_controller.Parameters.Gravity = 0;
		}
	}
	
	IEnumerator Flicker(Color initialColor, Color flickerColor, float flickerSpeed)
	{
		for(var n = 0; n < 10; n++)
		{
			GetComponent<Renderer>().material.color = initialColor;
			yield return new WaitForSeconds (flickerSpeed);
			GetComponent<Renderer>().material.color = flickerColor;
			yield return new WaitForSeconds (flickerSpeed);
		}
		GetComponent<Renderer>().material.color = initialColor;

		// katman 12 (Mermiler) ve 13 (Düşmanlar) ile karakterin tekrar çarpışmasını sağlar
		Physics2D.IgnoreLayerCollision(9,12,false);
		Physics2D.IgnoreLayerCollision(9,13,false);
	}
	
	public void Kill()
	{
		_controller.HandleCollisions=false;
		GetComponent<Collider2D>().enabled=false;
		BehaviorState.IsDead=true;
		Health=0;
		_controller.SetForce(new Vector2(0,10));		
	}
	
	public void Disable()
	{
		enabled=false;
		_controller.enabled=false;
		GetComponent<Collider2D>().enabled=false;		
	}

	/// <param name="spawnPoint">Yeniden doğmanın yeri.</param>
	public void RespawnAt(Transform spawnPoint)
	{
		if(!_isFacingRight)
		{
			Flip ();
		}
		BehaviorState.IsDead=false;
		GetComponent<Collider2D>().enabled=true;
		_controller.HandleCollisions=true;
		transform.position=spawnPoint.position;
		Health=BehaviorParameters.MaxHealth;
	}

	/// <param name="damage">Uygulanan hasar.</param>
	/// <param name="instigator">Hasar kışkırtıcı.</param>
	public void TakeDamage(int damage,GameObject instigator)
	{
		if (PlayerHitSfx!=null)
			SoundManager.Instance.PlaySound(PlayerHitSfx,transform.position);	
		
		Instantiate(HurtEffect,transform.position,transform.rotation);
		Physics2D.IgnoreLayerCollision(9,12,true);
		Physics2D.IgnoreLayerCollision(9,13,true);
		Color initialColor = GetComponent<Renderer>().material.color;
		Color flickerColor = new Color32(255, 20, 20, 255); 
		StartCoroutine(Flicker(initialColor,flickerColor,0.05f));		
		Health -= damage;
		if (Health<=0)
		{
			LevelManager.Instance.KillPlayer();
		}
	}

	/// <param name="health">Karakterin aldığı sağlık.</param>
	/// <param name="instigator">Karaktere sağlık veren şey.</param>
	public void GiveHealth(int health,GameObject instigator)
	{
		Health = Mathf.Min (Health + health,BehaviorParameters.MaxHealth);
	}
	
	private void Flip()
	{
		transform.localScale = new Vector3(-transform.localScale.x,transform.localScale.y,transform.localScale.z);
		_isFacingRight = transform.localScale.x > 0;
		
		
		if (_jetpack!=null)
		{
			if (_jetpack.Jetpack!=null)
				_jetpack.Jetpack.transform.eulerAngles = new Vector3(_jetpack.Jetpack.transform.eulerAngles.x,_jetpack.Jetpack.transform.eulerAngles.y+180,_jetpack.Jetpack.transform.eulerAngles.z);
		}
		if (_shoot != null) 
		{
			_shoot.Flip();		
		}
	}
	
	/// <param name="other">The other collider.</param>
	public void OnTriggerEnter2D(Collider2D collider)
	{
		
		var parameters = collider.gameObject.GetComponent<CorgiControllerPhysicsVolume2D>();
		if (parameters == null)
			return;
		_overrideBehaviorParameters = parameters.BehaviorParameters;
	}	
	
	/// <param name="other">The other collider.</param>
	public void OnTriggerStay2D( Collider2D collider )
	{
	}	
	
	/// <param name="collider">The other collider.</param>
	public void OnTriggerExit2D(Collider2D collider)
	{		
		
		var parameters = collider.gameObject.GetComponent<CorgiControllerPhysicsVolume2D>();
		if (parameters == null)
			return;

		// diğer çarpıştırıcının davranış parametreleri varsa, bizimkini sıfırlarız
		_overrideBehaviorParameters = null;
	}	
}
