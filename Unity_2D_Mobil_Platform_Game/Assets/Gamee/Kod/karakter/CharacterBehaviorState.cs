using UnityEngine;
using System.Collections;

public class CharacterBehaviorState 
{
	/// karakter şu anda atlayabilir mi?
	public bool CanJump{get;set;}
	///karakter şu anda ateş edebilir mi?
	public bool CanShoot{get;set;}
	/// karakter şu anda yakın dövüş saldırısını kullanabilir mi?
	public bool CanMelee{get;set;}
	///karakter şu anda fırlayabiliyor mu?
	public bool CanDash{get;set;}
	/// karakter şu anda özgürce hareket edebiliyor mu?
	public bool CanMoveFreely{get;set;}
	/// karakter jetpack olabilir mi?
	public bool CanJetpack{ get; set; }
	/// karaktere kalan atlama sayısı
	public int NumberOfJumpsLeft;
	/// karakter şu anda atılsa true
	public bool Dashing{get;set;}
	/// karakter şu anda çalışıyorsa true
	public bool Running{get;set;}
	/// karakter şu anda çömeliyorsa true
	public bool Crouching{get;set;}
	/// önceki kare sırasında karakter çömelmişse true
	public bool CrouchingPreviously{get;set;}
	/// karakter şu anda yukarı bakıyorsa true
	public bool LookingUp{get;set;}
	/// karakter şu anda duvara yapışıyorsa true
	public bool WallClinging{get;set;}
	/// karakter şu anda jetpack yapıyorsa true
	public bool Jetpacking{get;set;}
	/// eğer karakter şu anda hızlı atlıyorsa true
	public bool Diving{get;set;}
	/// karakter şu anda silahını ateşliyorsa true
	public bool Firing{get;set;}
	/// karakter çekimi durdurduğunda bir kare için gerçek olur
	public bool FiringStop{get;set;}
	/// karakter şu anda yakın dövüş saldırısını kullanarak saldırıyorsa true
	public bool MeleeAttacking{get;set;}
	/// karakter bir merdivenle çarpışıyorsa true
	public bool LadderColliding{get;set;}
	/// karakter bir merdivenin tepesiyle çarpışıyorsa true
	public bool LadderTopColliding{get;set;}
	/// karakter tepesinden bir merdiveni tırmanmaya başladıysa true
	public bool LadderEnteredViaTop{get;set;}
	/// karakter merdivene tırmanıyorsa true
	public bool LadderClimbing{get;set;}
	/// karakterin mevcut merdiven tırmanma hızı
	public float LadderClimbingSpeed{get;set;}
	/// ateşleme yönü - 1: üst, 2: sağ üst, 3: sağ, 4: sağ alt, 5: alt, 6: sol alt, 7: sol, 8: sol üst
	public int FiringDirection{get;set;}
	/// karakter şu anda ölüyse true
	public bool IsDead{get;set;}
	/// kalan jetpack yakıt süresi (saniye cinsinden)
	public float JetpackFuelDurationLeft{get;set;}

	/// <summary>
	/// Tüm durumları varsayılan değerlerine başlatır
	/// </summary>
	public void Initialize()
	{				
		CanMoveFreely = true;
		CanDash = true;
		CanShoot = true;
		CanMelee = true;
		CanJetpack = true;
		Dashing = false;
		Running = false;
		Crouching = false;
		CrouchingPreviously=false;
		LookingUp = false;
		WallClinging = false;
		Jetpacking = false;
		Diving = false;
		LadderClimbing=false;
		LadderColliding=false;
		LadderEnteredViaTop=false;
		LadderTopColliding=false;
		LadderClimbingSpeed=0f;
		Firing = false;
		FiringStop = false;
		FiringDirection = 3;
		MeleeAttacking=false;
	}	
}
