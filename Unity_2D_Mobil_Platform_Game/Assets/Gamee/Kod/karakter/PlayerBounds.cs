using UnityEngine;
using System.Collections;
/// <summary>
///Bu s�n�f, oyuncu seviye s�n�rlar�na ula�t���nda ne olaca��n� ele al�r.
/// </summary>
public class PlayerBounds : MonoBehaviour 
{
	public enum BoundsBehavior 
	{
		Nothing,
		Constrain,
		Kill
	}
	/// �st seviye s�n�r�na ula�t���nda oyuncuya ne yapmal�
	public BoundsBehavior Yukari;
	/// oyuncu alt seviye s�n�r�na ula�t���nda ne yapmal�
	public BoundsBehavior Asagi;
	/// sol seviye s�n�r�na ula�t���nda oyuncuya ne yapmal�
	public BoundsBehavior Sol;
	/// do�ru seviye s�n�r�na ula�t���nda oyuncuya ne yapmal�
	public BoundsBehavior Sag;
	
	private BoxCollider2D _bounds;
	private CharacterBehavior _player;
	private BoxCollider2D _boxCollider;
	
	public void Start () 
	{
		_player=GetComponent<CharacterBehavior>();
		_boxCollider=GetComponent<BoxCollider2D>();
		_bounds=GameObject.FindGameObjectWithTag("LevelBounds").GetComponent<BoxCollider2D>();
	}

	/// <summary>
	/// Her karede, oyuncunun bir seviye s�n�r� ile �arp���p �arp��mad���n� kontrol ediyoruz
	/// </summary>
	public void Update () 
	{
		// oyuncu �l�rse hi�bir �ey yapmay�z
		if (_player.BehaviorState.IsDead)
			return;

		// oyuncunun boxcollider boyutunu hesapl�yoruz
		var colliderSize =new Vector2(
			_boxCollider.size.x * Mathf.Abs (transform.localScale.x),
			_boxCollider.size.y * Mathf.Abs (transform.localScale.y))/2;

		// oyuncu bir s�n�ra ula�t���nda, belirtilen ba�l� davran��� uygular�z
		if (Yukari != BoundsBehavior.Nothing && transform.position.y + colliderSize.y > _bounds.bounds.max.y)
			ApplyBoundsBehavior(Yukari, new Vector2(transform.position.x,_bounds.bounds.max.y - colliderSize.y));
		
		if (Asagi != BoundsBehavior.Nothing && transform.position.y - colliderSize.y < _bounds.bounds.min.y)
			ApplyBoundsBehavior(Asagi, new Vector2(transform.position.x, _bounds.bounds.min.y + colliderSize.y));
		
		if (Sag != BoundsBehavior.Nothing && transform.position.x + colliderSize.x > _bounds.bounds.max.x)
			ApplyBoundsBehavior(Sag, new Vector2(_bounds.bounds.max.x - colliderSize.x,transform.position.y));		
		
		if (Sol != BoundsBehavior.Nothing && transform.position.x - colliderSize.x < _bounds.bounds.min.x)
			ApplyBoundsBehavior(Sol, new Vector2(_bounds.bounds.min.x + colliderSize.x,transform.position.y));
		
	}

	/// <summary>
	/// Oyuncuya belirtilen ba�l� davran��� uygular
	/// </summary>
	/// <param name="behavior">Behavior.</param>
	/// <param name="constrainedPosition">K�s�tl� pozisyon.</param>
	private void ApplyBoundsBehavior(BoundsBehavior behavior, Vector2 constrainedPosition)
	{
		if (behavior== BoundsBehavior.Kill)
		{
			LevelManager.Instance.KillPlayer ();
		}	
		transform.position = constrainedPosition;	
	}
}
