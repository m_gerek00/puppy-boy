using UnityEngine;
using System.Collections;

/// <summary>
/// Karakterinizin mevcut karede bir �ey yap�p yapmad���n� kontrol etmek i�in kullanabilece�iniz �e�itli durumlar
/// </summary>

public class CorgiControllerState 
{
	/// karakter �arp���yor mu?
	public bool IsCollidingRight { get; set; }
	/// karakter sola m� �arp�yor?
	public bool IsCollidingLeft { get; set; }
	/// karakter �st�ndeki bir �eye �arp�yor mu?
	public bool IsCollidingAbove { get; set; }
	/// karakter �st�ndeki bir �eye �arp�yor mu?
	public bool IsCollidingBelow { get; set; }
	/// karakter herhangi bir �eye �arp�yor mu?
	public bool HasCollisions { get { return IsCollidingRight || IsCollidingLeft || IsCollidingAbove || IsCollidingBelow; }}

	/// karakter yoku� a�a�� m� hareket ediyor?
	public bool IsMovingDownSlope { get; set; }
	/// karakter bir yoku� yukar� m� hareket ediyor?
	public bool IsMovingUpSlope { get; set; }
	/// karakterin a��da hareket etti�i e�imi verir
	public float SlopeAngle { get; set; }
	/// e�im a��s� �zerinde y�r�mek i�in uygunsa do�ru d�nd�r�r
	public bool SlopeAngleOK { get; set; }

	///Karakter topraklanm�� m�?
	public bool IsGrounded { get { return IsCollidingBelow; } }
	/// karakter �u anda d���yor mu?
	public bool IsFalling { get; set; }
	/// karakter son karede topraklanm�� m�yd�?
	public bool WasGroundedLastFrame { get ; set; }
	/// karakter az �nce toprakland� m�?
	public bool JustGotGrounded { get ; set;  }

	/// <summary>
	/// T�m �arp��ma durumlar�n� yanl�� olarak s�f�rlay�n
	/// </summary>
	public void Reset()
	{
		IsMovingUpSlope =
		IsMovingDownSlope =
		IsCollidingLeft = 
		IsCollidingRight = 
		IsCollidingAbove =
		SlopeAngleOK =
		JustGotGrounded = false;
		IsFalling=true;
		SlopeAngle = 0;
	}

	/// <summary>
	/// �arp��ma durumlar�n� seri hale getirir
	/// </summary>
	/// <returns>A <see cref="System.String"/> mevcut �arp��ma durumlar�n� temsil eden.</returns>
	public override string ToString ()
	{
		return string.Format("(controller: r:{0} l:{1} a:{2} b:{3} down-slope:{4} up-slope:{5} angle: {6}",
		IsCollidingRight,
		IsCollidingLeft,
		IsCollidingAbove,
		IsCollidingBelow,
		IsMovingDownSlope,
		IsMovingUpSlope,
		SlopeAngle);
	}	
}