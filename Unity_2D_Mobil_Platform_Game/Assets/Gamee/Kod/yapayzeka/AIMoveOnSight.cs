﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Bu bileşeni bir CorgiController2D'ye ekleyin ve oynatıcınızı görünürde öldürmeye çalışacaktır.
/// </summary>
public class AIMoveOnSight : MonoBehaviour 
{

	/// Temsilcinin hızı
	public float Speed=3f;
	/// Yapay zekanın oyuncuyu görebileceği maksimum mesafe
	public float ViewDistance = 10f;

	private float _canFireIn;
	private Vector2 _direction;
	private CorgiController _controller;
	private Animator _animator;

	void Start () 
	{
		// CorgiController2D bileşenini alıyoruz
		_controller = GetComponent<CorgiController>();
		// karakterin animatörünü alıyoruz
		_animator = GetComponent<Animator>();
		_direction=Vector2.right;
	}

	/// Her kare, oynatıcıyı kontrol edin ve onu öldürmeye çalışın
	void Update () 
	{
		bool hit=false;

		// Bir Oyuncuyu kontrol etmek için temsilcinin soluna bir ışın fırlatıyoruz
		Vector2 raycastOrigin = new Vector2(transform.position.x,transform.position.y+(transform.localScale.y/2));		
		RaycastHit2D raycast = CorgiRayCast(raycastOrigin,-Vector2.right,ViewDistance,1<<LayerMask.NameToLayer("Player"),true,Color.gray);
		if (raycast)
		{
			hit=true;
			_direction=-Vector2.right;
		}

		// Bir Oyuncuyu kontrol etmek için temsilcinin sağına bir ışın gönderiyoruz
		raycastOrigin = new Vector2(transform.position.x,transform.position.y+(transform.localScale.y/2));		
		raycast = CorgiRayCast(raycastOrigin,Vector2.right,ViewDistance,1<<LayerMask.NameToLayer("Player"),true,Color.gray);
		if (raycast)
		{
			hit=true;
			_direction=Vector2.right;
		}

		// ışın oyuncuya çarptıysa, ajanı bu yönde hareket ettiririz
		if (hit)
			_controller.SetHorizontalForce(_direction.x * Speed);		
		else
			_controller.SetHorizontalForce(0);
			
		if (_direction==Vector2.right)			
			transform.localScale=new Vector3(-Mathf.Abs(transform.localScale.x),transform.localScale.y,transform.localScale.z);
		else
            transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);

		// ajan bir şeyle çarpışıyorsa, onu tersine çevirin
		if ((_direction.x < 0 && _controller.State.IsCollidingLeft) || (_direction.x > 0 && _controller.State.IsCollidingRight))
		{
			_direction = -_direction;
			transform.localScale = new Vector3(-transform.localScale.x,transform.localScale.y,transform.localScale.z);
		}		
		
		if (_animator!=null)
			_animator.SetFloat("Speed", Mathf.Abs(_controller.Speed.x));
			
	}

	/// <summary>
	/// Bir hata ayıklama ışını çizer ve gerçek ışın yayını yapar
	/// </summary>
	/// <returns>The cast.</returns>
	/// <param name="rayOriginPoint">Işın başlangıç noktası.</param>
	/// <param name="rayDirection">Işın yönü.</param>
	/// <param name="rayDistance">Işın Mesafesi.</param>
	/// <param name="mask">Mask.</param>
	/// <param name="debug">If set to <c>true</c> debug.</param>
	/// <param name="color">Renk.</param>
	private RaycastHit2D CorgiRayCast(Vector2 rayOriginPoint, Vector2 rayDirection, float rayDistance, LayerMask mask,bool debug,Color color)
	{			
		Debug.DrawRay( rayOriginPoint, rayDirection*rayDistance, color );
		return Physics2D.Raycast(rayOriginPoint,rayDirection,rayDistance,mask);		
	}
}
