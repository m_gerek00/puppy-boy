using UnityEngine;
using System.Collections;
/// <summary>
/// Bu s�n�f, yollu bir merminin hareketini y�netir
/// </summary>
public class PathedProjectile : MonoBehaviour
{
	/// Nesne yok edildi�inde somutla�t�r�lacak etki
	public GameObject DestroyEffect;
	/// merminin hedefi
	private Transform _destination;
	/// hareket h�z�
	private float _speed;

	/// <summary>
	/// Belirtilen hedefi ve h�z� ba�lat�r.
	/// </summary>
	/// <param name="destination">Hedef.</param>
	/// <param name="speed">H�z.</param>
	public void Initialize(Transform destination, float speed)
	{
		_destination=destination;
		_speed=speed;
	}

	/// <summary>
	/// Her kare, merminin konumunu hedefine g�t�r�yorum
	/// </summary>
	void Update () 
	{
		transform.position=Vector3.MoveTowards(transform.position,_destination.position,Time.deltaTime * _speed);
		var distanceSquared = (_destination.transform.position - transform.position).sqrMagnitude;
		if(distanceSquared > .01f * .01f)
			return;
		
		if (DestroyEffect!=null)
		{
			Instantiate(DestroyEffect,transform.position,transform.rotation);
		}
		
		Destroy(gameObject);
	}	
}
