﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Kendi başına dönmesini sağlamak için bu sınıfı bir GameObject'e ekleyin
/// </summary>
public class AutoRotate : MonoBehaviour 
{
	/// Dönüş hızı. Pozitif, saat yönünde, negatif ise saat yönünün tersi anlamına gelir.
	public float rotationSpeed = 100f;

	/// <summary>
	/// Nesnenin her karede merkezinde dönmesini sağlar.
	/// </summary>
	void Update () 
	{
		transform.Rotate(rotationSpeed*Vector3.forward*Time.deltaTime);
	}
}
