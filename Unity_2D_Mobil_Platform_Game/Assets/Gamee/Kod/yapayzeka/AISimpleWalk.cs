using UnityEngine;
using System.Collections;
/// <summary>
/// Bu bile�eni bir CorgiController2D'ye ekleyin ve oynat�c�n�z� g�r�n�rde �ld�rmeye �al��acakt�r.
/// </summary>
public class AISimpleWalk : MonoBehaviour,IPlayerRespawnListener
{
	/// Temsilcinin h�z�
	public float Speed;
	/// �lk y�n
	public bool GoesRightInitially=true;
	

    private CorgiController _controller;
    private Vector2 _direction;
	private Vector2 _startPosition;
	
	/// <summary>
	/// Ba�latma
	/// </summary>
	public void Start ()
    {
		// CorgiController2D bile�enini al�yoruz
		_controller = GetComponent<CorgiController>();
		// ba�lang�� konumunu ba�lat
		_startPosition = transform.position;
		// y�n� ba�lat
		_direction = GoesRightInitially ? Vector2.right : -Vector2.right;
	}

	/// <summary>
	/// Her kare, ajan� hareket ettirir ve oyuncuya ate� edip edemeyece�ini kontrol eder.
	/// </summary>
	public void Update () 
	{

		// temsilciyi mevcut y�n�nde hareket ettirir
		_controller.SetHorizontalForce(_direction.x * Speed);

		// ajan bir �eyle �arp���yorsa, onu tersine �evirin
		if ((_direction.x < 0 && _controller.State.IsCollidingLeft) || (_direction.x > 0 && _controller.State.IsCollidingRight))
		{
			_direction = -_direction;
			transform.localScale = new Vector3(-transform.localScale.x,transform.localScale.y,transform.localScale.z);
		}
	}

	/// <summary>
	/// Oyuncu yeniden do�du�unda, bu ajan� eski durumuna getiririz.
	/// </summary>
	/// <param name="checkpoint">Checkpoint.</param>
	/// <param name="player">Player.</param>
	public void onPlayerRespawnInThisCheckpoint (CheckPoint checkpoint, CharacterBehavior player)
	{
		_direction = new Vector2(-1,0);
		transform.localScale=new Vector3(1,1,1);
		transform.position=_startPosition;
		gameObject.SetActive(true);
	}

}
