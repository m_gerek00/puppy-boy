﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Bu bileşeni bir CorgiController2D'ye ekleyin ve oynatıcınızı görünürde öldürmeye çalışacaktır.
/// </summary>
public class AIShootOnSight : MonoBehaviour 
{

	/// Ateş hızı (saniye cinsinden)
	public float FireRate = 1;
	/// Ajan tarafından atılan mermi türü
	public Projectile Projectile;
	/// AI'nın oyuncuya ateş edebileceği maksimum mesafe
	public float ShootDistance = 10f;

	
	private float _canFireIn;
	private Vector2 _direction;
	private Vector2 _directionLeft;
	private Vector2 _directionRight;
	private CorgiController _controller;

	void Start () 
	{
		_directionLeft = new Vector2(-1,0);
		_directionRight = new Vector2(1,0);
		// CorgiController2D bileşenini alıyoruz
		_controller = GetComponent<CorgiController>();
	}

	/// Her kare, oynatıcıyı kontrol edin ve onu öldürmeye çalışın
	void Update () 
	{
		// ateş cooldown
		if ((_canFireIn-=Time.deltaTime) > 0)
		{
			return;
		}

		// AI'nın yönünü belirlemek
		if (transform.localScale.x < 0) 
		{
			_direction=-_directionLeft;
		}
		else
		{
			_direction=-_directionRight;
		}

		// Bir Oyuncuyu kontrol etmek için temsilcinin önüne bir ışın atarız
		Vector2 raycastOrigin = new Vector2(transform.position.x,transform.position.y-(transform.localScale.y/2));
		RaycastHit2D raycast = Physics2D.Raycast(raycastOrigin,_direction,ShootDistance,1<<LayerMask.NameToLayer("Player"));
		if (!raycast)
			return;

		// ışın oyuncuya çarptıysa, bir mermi ateşleriz
		Projectile projectile = (Projectile)Instantiate(Projectile, transform.position,transform.rotation);
		projectile.Initialize(gameObject,_direction,_controller.Speed);
		_canFireIn=FireRate;
	}
}
