using UnityEngine;
using System.Collections;

public class AIFollower : MonoBehaviour 
{
	public bool AgentFollowsPlayer{get;set;}
	
	
	public float RunDistance = 10f;
	public float WalkDistance = 5f;
	public float StopDistance = 1f;
	public float JetpackDistance = 0.2f;
	 
	private Transform _target;
	private CharacterBehavior _playerComponent;
	private CorgiController _controller;
	private CharacterJetpack _jetpack;
	private float _speed;
	private float _direction;
	
	void Start () 
	{
		_target=GameManager.Instance.Player.transform;
		_playerComponent=(CharacterBehavior)GetComponentInParent<CharacterBehavior>();
		_controller=(CorgiController)GetComponentInParent<CorgiController>();
		_jetpack = (CharacterJetpack)GetComponentInParent<CharacterJetpack>();
		AgentFollowsPlayer=true;
	}
	
	void Update () 
	{
		if (!AgentFollowsPlayer)
			return;

		if ( (_playerComponent==null) || (_controller==null) )
			return;
	
		float distance = Mathf.Abs(_target.position.x - transform.position.x);
					
		_direction = _target.position.x>transform.position.x ? 1f : -1f;
		
		if (distance>RunDistance)
		{
			_speed=1;
			_playerComponent.RunStart();
		}
		else
		{
			 //hızlanma
			_playerComponent.RunStop();
		}
		if (distance<RunDistance && distance>WalkDistance)
		{
			// yürüme
			_speed=1;
		}
		if (distance<WalkDistance && distance>StopDistance)
		{
			// yavaş yürüme
			_speed=distance/WalkDistance;
		}
		if (distance<StopDistance)
		{
			// durma
			_speed=0f;
		}
		
		_playerComponent.SetHorizontalMove(_speed*_direction);
		
		if (_controller.State.IsCollidingRight || _controller.State.IsCollidingLeft)
		{
			_playerComponent.JumpStart();
		}
		
		if (_jetpack!=null)
		{
			if (_target.position.y>transform.position.y+JetpackDistance)
			{
				_jetpack.JetpackStart();
			}
			else
			{
				_jetpack.JetpackStop();
			}
		}
	}
}
