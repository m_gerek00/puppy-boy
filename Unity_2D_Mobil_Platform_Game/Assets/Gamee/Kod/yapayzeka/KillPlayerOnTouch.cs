using UnityEngine;
using System.Collections;
/// <summary>
/// Dokunuldu�unda oyuncuyu �ld�rmesi i�in Collider2D Trigger olarak ayarlanm�� bir GameObject'e bunu ekleyin.
/// </summary>
public class KillPlayerOnTouch : MonoBehaviour 
{
	/// <summary>
	/// Bir �arp��ma tetiklendi�inde, �arp��an �eyin ger�ekten oyuncu olup olmad���n� kontrol edin. Cevab�n�z evet ise, �ld�r�n.
	/// </summary>
	/// <param name="collider">KillPlayerOnTouch nesnesiyle �arp��an nesne.</param>
	public void OnTriggerEnter2D(Collider2D collider)
	{
		var player = collider.GetComponent<CharacterBehavior>();
		if (player==null)
			return;
		
		LevelManager.Instance.KillPlayer();
	}
}
