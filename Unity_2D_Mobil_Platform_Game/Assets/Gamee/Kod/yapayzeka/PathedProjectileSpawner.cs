﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Yollu mermiler ortaya çıkarır
/// </summary>
public class PathedProjectileSpawner : MonoBehaviour 
{
	/// yollu merminin hedefi
	public Transform Destination;
	/// ortaya çıkacak mermiler
	public PathedProjectile Projectile;
	/// her yumurtlamada somutlaştırma etkisi
	public GameObject SpawnEffect;
	/// mermilerin hızı
	public float Speed;
	/// yumurtlama sıklığı
	public float FireRate;
	
	private float _nextShotInSeconds;
	
	/// <summary>
	/// Başlat
	/// </summary>
	void Start () 
	{
		_nextShotInSeconds=FireRate;
	}

	/// <summary>
	/// Her kare, yeni bir mermi başlatmamız gerekip gerekmediğini kontrol ediyoruz.
	/// </summary>
	void Update () 
	{
		if((_nextShotInSeconds -= Time.deltaTime)>0)
			return;
			
		_nextShotInSeconds = FireRate;
		var projectile = (PathedProjectile) Instantiate(Projectile, transform.position,transform.rotation);
		projectile.Initialize(Destination,Speed);
		
		if (SpawnEffect!=null)
		{
			Instantiate(SpawnEffect,transform.position,transform.rotation);
		}
	}

	/// <summary>
	/// Debug mod
	/// </summary>
	public void OnDrawGizmos()
	{
		if (Destination==null)
			return;
		
		Gizmos.color=Color.red;
		Gizmos.DrawLine(transform.position,Destination.position);
	}
}
