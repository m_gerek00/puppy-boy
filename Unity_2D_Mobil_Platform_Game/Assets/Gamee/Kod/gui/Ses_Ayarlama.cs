﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ses_Ayarlama : MonoBehaviour
{

    public Slider Ses;
    public AudioSource SesKontrol;
    void Update()
    {
        SesKontrol.volume = Ses.value;
    }
}
