using UnityEngine;
using System.Collections;
public class HealthBar : MonoBehaviour 
{
	public Transform ForegroundSprite;
	/// max canın rengi
	public Color MaxHealthColor = new Color(255/255f, 63/255f, 63/255f);
	/// min canın rengi
	public Color MinHealthColor = new Color(64/255f, 137/255f, 255/255f);
	
	private CharacterBehavior _character;

	void Start()
	{
		_character = GameManager.Instance.Player;
	}

	/// <summary>
	/// Her kare, ön plan hareketli grafiğinin genişliğini karakterin sağlığına uyacak şekilde ayarlar.
	/// </summary>
	public void Update()
	{
		if (_character==null)
			return;
		var healthPercent = _character.Health / (float) _character.BehaviorParameters.MaxHealth;
		ForegroundSprite.localScale = new Vector3(healthPercent,1,1);
		
	}
	
}
