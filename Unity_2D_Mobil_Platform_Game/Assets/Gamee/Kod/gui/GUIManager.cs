﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GUIManager : MonoBehaviour 
{
	public GameObject HUD;
	public GameObject PauseMenu;	
	public GameObject TimeSplash;
	public Text PointsText;
	public Text LevelText;
	public Image Fader;
	public GameObject JetPackBar;
	
	private static GUIManager _instance;

	// Tekli desen
	public static GUIManager Instance
	{
		get
		{
			if(_instance == null)
				_instance = GameObject.FindObjectOfType<GUIManager>();
			return _instance;
		}
	}

	public void Start()
	{
		RefreshPoints();
		
	}

	/// <param name="state"><c> true </c> olarak ayarlanırsa, HUD'yi etkinleştirir, aksi takdirde kapatır.</param>
	public void SetHUDActive(bool state)
	{
		HUD.SetActive(state);
		PointsText.enabled=state;
		LevelText.enabled=state;
	}

	/// <param name="state"><c> true </c> olarak ayarlanırsa, duraklatmayı ayarlar.</param>
	public void SetPause(bool state)
	{
		PauseMenu.SetActive(state);
	}

	/// <param name="state">If set to <c>true</c>, pause yapıcaz.</param>
	public void SetJetpackBar(bool state)
	{
		JetPackBar.SetActive(state);
	}

	/// <param name="state"><c> true </c> olarak ayarlanırsa, zaman sıçramasını açar.</param>
	public void SetTimeSplash(bool state)
	{
		TimeSplash.SetActive(state);
	}
	
	public void RefreshPoints()
	{
		PointsText.text=GameManager.Instance.Points.ToString("000000")+"TL";		
	}
	
	public void SetLevelName(string name)
	{
		LevelText.text=name;		
	}

	/// <param name="state"><c> true </c> olarak ayarlanırsa, kısıcıyı yavaşlatır, aksi takdirde <c> false </c> ise söner.</param>
	public void FaderOn(bool state,float duration)
	{
		Fader.gameObject.SetActive(true);
		if (state)
			StartCoroutine(FadeTo(Fader,1f, duration));
		else
			StartCoroutine(FadeTo(Fader,0f, duration));
	}
	
	IEnumerator FadeTo(Image target, float opacity, float duration)
	{
		//float alpha = target.renderer.material.color.a;
		float alpha = target.color.a;
		
		for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / duration)
		{
			Color newColor = new Color(0, 0, 0, Mathf.SmoothStep(alpha,opacity,t));
			target.color=newColor;
			//target.renderer.material.color = newColor;
			yield return null;
		}
	}
}
