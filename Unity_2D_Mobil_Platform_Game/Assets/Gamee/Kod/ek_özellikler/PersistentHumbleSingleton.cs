﻿using UnityEngine;
using System;

public class PersistentHumbleSingleton<T> : MonoBehaviour	where T : Component
{
	private static T _instance;
	public float InitializationTime;
	
	/// <value>Örnek.</value>
	public static T Instance 
	{
		get 
		{
			if (_instance == null) 
			{
				_instance = FindObjectOfType<T> ();
				if (_instance == null) 
				{
					GameObject obj = new GameObject ();
					obj.hideFlags = HideFlags.HideAndDontSave;
					_instance = obj.AddComponent<T> ();
				}
			}
			return _instance;
		}
	}

	/// <summary>
	/// Uyanıkken, sahnede nesnenin zaten bir kopyası olup olmadığını kontrol ederiz. Eğer varsa, onu yok ederiz.
	/// </summary>
	public virtual void Awake ()
	{
		InitializationTime=Time.time;
		
		DontDestroyOnLoad (this.gameObject);
		// aynı türden mevcut nesneleri kontrol ediyoruz
		T[] check = FindObjectsOfType<T>();
		foreach (T searched in check)
		{
			if (searched!=this)
			{
				// aynı türde başka bir nesne bulursak (bu değil) ve mevcut nesnemizden daha yaşlıysa, onu yok ederiz.
				if (searched.GetComponent<PersistentHumbleSingleton<T>>().InitializationTime<InitializationTime)
				{
					Destroy (searched.gameObject);
				}
			}
		}		
		
		if (_instance == null) 
		{
			_instance = this as T;
		} 	
	}
}