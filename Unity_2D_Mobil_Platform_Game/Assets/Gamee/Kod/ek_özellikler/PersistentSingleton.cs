﻿using UnityEngine;

public class PersistentSingleton<T> : MonoBehaviour	where T : Component
{
	private static T _instance;

	/// <value>Örnek.</value>
	public static T Instance 
	{
		get 
		{
			if (_instance == null) 
			{
				_instance = FindObjectOfType<T> ();
				if (_instance == null) 
				{
					GameObject obj = new GameObject ();
					obj.hideFlags = HideFlags.HideAndDontSave;
					_instance = obj.AddComponent<T> ();
				}
			}
			return _instance;
		}
	}

	/// <summary>
	/// Uyanıkken, sahnede nesnenin zaten bir kopyası olup olmadığını kontrol ederiz. Eğer varsa, onu yok ederiz.
	/// </summary>
	public virtual void Awake ()
	{
		DontDestroyOnLoad (this.gameObject);
		if (_instance == null) 
		{
			_instance = this as T;
		} 
		else 
		{
			Destroy (gameObject);
		}
	}
}