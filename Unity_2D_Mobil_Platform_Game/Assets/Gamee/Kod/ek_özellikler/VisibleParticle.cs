﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Sınıflandırma katmanlarını zorlamak için bu sınıfı parçacıklara ekler
/// </summary>
public class VisibleParticle : MonoBehaviour {

	/// <summary>
	/// Parçacık sisteminin oluşturucusunu Görünür Parçacıklar sıralama katmanına ayarlar
	/// </summary>
	void Start () 
	{
		GetComponent<ParticleSystem>().GetComponent<Renderer>().sortingLayerName = "VisibleParticles";
	}
	
}
