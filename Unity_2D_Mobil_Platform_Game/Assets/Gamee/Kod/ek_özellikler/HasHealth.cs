using UnityEngine;
using System.Collections;

public class HasHealth : MonoBehaviour, CanTakeDamage
{
	public int Health;
	public int PointsWhenDestroyed;
	public GameObject HurtEffect;
	public GameObject DestroyEffect;

	/// <param name="damage">Damage.</param>
	/// <param name="instigator">Instigator.</param>
	public void TakeDamage(int damage,GameObject instigator)
	{
		// nesne hasar ald���nda, onun zarar verici etkisini somutla�t�r�r�z
		Instantiate(HurtEffect,instigator.transform.position,transform.rotation);
		// ve belirtilen sa�l�k miktar�n� kald�r�n
		Health -= damage;
		// nesnenin art�k sa�l��� yoksa, onu yok ederiz
		if (Health<=0)
		{
			DestroyObject();
            return;
		}

		// Karakterin sprite titremesini sa�l�yoruz
		Color initialColor = GetComponent<Renderer>().material.color;
        Color flickerColor = new Color32(255, 20, 20, 255);
        StartCoroutine(Flicker(initialColor, flickerColor, 0.02f));	
	}

    IEnumerator Flicker(Color initialColor, Color flickerColor, float flickerSpeed)
    {
        for (var n = 0; n < 10; n++)
        {
            GetComponent<Renderer>().material.color = initialColor;
            yield return new WaitForSeconds(flickerSpeed);
            GetComponent<Renderer>().material.color = flickerColor;
            yield return new WaitForSeconds(flickerSpeed);
        }
        GetComponent<Renderer>().material.color = initialColor;

		// katman 12 (Mermiler) ve 13 (D��manlar) ile karakterin tekrar �arp��mas�n� sa�lar
		Physics2D.IgnoreLayerCollision(9, 12, false);
        Physics2D.IgnoreLayerCollision(9, 13, false);
    }
	

	private void DestroyObject()
	{
		// yok etme etkisini somutla�t�r�r
		if (DestroyEffect!=null)
		{
			var instantiatedEffect=(GameObject)Instantiate(DestroyEffect,transform.position,transform.rotation);
            instantiatedEffect.transform.localScale = transform.localScale;
		}
		// Gerekirse puan ekler.
		if (PointsWhenDestroyed != 0)
		{
			GameManager.Instance.AddPoints(PointsWhenDestroyed);
		}
		// nesneyi yok eder
		gameObject.SetActive(false);
	}
}
