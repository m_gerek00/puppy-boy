﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Bu sınıfı bir ParticleSystem'e ekleyin, böylece yaymayı durdurduğunda otomatik olarak yok olur.
/// ParticleSystem'inizin döngü yapmadığından emin olun, aksi takdirde bu komut dosyası işe yaramaz
/// </summary>
public class AutoDestroyParticleSystem : MonoBehaviour 
{
	/// ParticleSystem'in üstünü de yok etmesi gerekiyorsa true
	public bool DestroyParent=false;
	
	private ParticleSystem _particleSystem;
	
	public void Start()
	{
		_particleSystem = GetComponent<ParticleSystem>();
	}
	
	public void Update()
	{	
		if (_particleSystem.isPlaying)
			return;
		
		if (transform.parent!=null)
		{
			if(DestroyParent)
			{	
				Destroy(transform.parent.gameObject);	
			}
		}
				
		Destroy (gameObject);
	}

}
