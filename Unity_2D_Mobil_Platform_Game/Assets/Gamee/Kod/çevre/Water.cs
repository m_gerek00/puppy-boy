using UnityEngine;
using System.Collections;

/// <summary>
/// Bu s�n�f� bir su k�tlesine ekler. Giri� / ��k��ta s��rama efektlerini idare edecek ve oyuncunun buradan atlamas�na izin verecektir.
/// </summary>
public class Water : MonoBehaviour 
{
	/// sudan ��kt���nda bir karaktere ekleme g�c�
	public float WaterExitForce=8f;
	/// karakter suya her girdi�inde veya ��kt���nda ortaya ��kacak etki
	public GameObject WaterEntryEffect;

	private int _numberOfJumpsSaved;

	/// <summary>
	/// Bir �ey suyla �arp��t���nda tetiklendi
	/// </summary>
	/// <param name="collider">Suyla �arp��an bir �ey.</param>
	public void OnTriggerEnter2D(Collider2D collider)
	{
		// suyla �arp��an nesnenin asl�nda bir karakter denetleyicisi ve bir karakter oldu�unu kontrol ediyoruz
		CharacterBehavior character = collider.GetComponent<CharacterBehavior>();
		if (character==null)
			return;		
		CorgiController controller = collider.GetComponent<CorgiController>();
		if (controller==null)
			return;		
			
		_numberOfJumpsSaved=character.BehaviorState.NumberOfJumpsLeft+1;
		Splash (character.transform.position);
	}

	/// <summary>
	/// Suda bir �ey kald���nda tetiklendi
	/// </summary>
	/// <param name="collider">Suyla �arp��an bir �ey.</param>
	public void OnTriggerStay2D(Collider2D collider)
	{
		// suyla �arp��an nesnenin asl�nda bir karakter denetleyicisi ve bir karakter oldu�unu kontrol ediyoruz
		CharacterBehavior character = collider.GetComponent<CharacterBehavior>();
		if (character==null)
			return;		
		CorgiController controller = collider.GetComponent<CorgiController>();
		if (controller==null)
			return;
	}

	/// <summary>
	/// Sudan bir �ey ��kt���nda tetiklendi
	/// </summary>
	/// <param name="collider">Suyla �arp��an bir �ey..</param>
	public void OnTriggerExit2D(Collider2D collider)
	{
		// suyla �arp��an nesnenin asl�nda bir karakter denetleyicisi ve bir karakter oldu�unu kontrol ediyoruz
		CharacterBehavior character = collider.GetComponent<CharacterBehavior>();
		if (character==null)
			return;		
		CorgiController controller = collider.GetComponent<CorgiController>();
		if (controller==null)
			return;

		// karakter art�k suyla �arp��mad���nda, suyla ilgili �e�itli durumlar�n� s�f�rl�yoruz
		character.BehaviorState.NumberOfJumpsLeft=_numberOfJumpsSaved;
		// biz de havaya kald�r�yoruz	
		Splash(character.transform.position);
		controller.SetVerticalForce(Mathf.Abs( WaterExitForce ));
	}

	/// <summary>
	/// Giri� noktas�nda su s��ramas� yarat�r
	/// </summary>
	private void Splash(Vector3 splashPosition)
	{
		
		Instantiate(WaterEntryEffect,splashPosition,Quaternion.identity);		
	}
}
