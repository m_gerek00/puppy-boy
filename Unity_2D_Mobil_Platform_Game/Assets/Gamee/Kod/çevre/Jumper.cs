using UnityEngine;
using System.Collections;
/// <summary>
/// Bu s�n�f� bir atlama platformu, trambolin veya her neyse yapmak i�in bir platforma ekleyin.
/// Kendisine dokunan herhangi bir karakteri otomatik olarak havaya iter.
/// </summary>
public class Jumper : MonoBehaviour 
{
	/// platform taraf�ndan ind�klenen atlama kuvveti
	public float JumpPlatformBoost = 40;
	/// <summary>
	/// Bir CorgiController platforma dokundu�unda, platforma dikey bir kuvvet uygulad���nda ve platformu havada itti�inde tetiklenir.
	/// </summary>
	/// <param name="controller">Platformla �arp��an corgi denetleyicisi.</param>

	public void OnTriggerEnter2D(Collider2D collider)
	{
		var controller=collider.GetComponent<CorgiController>();
		if (controller==null)
			return;
		
		controller.SetVerticalForce(Mathf.Sqrt( 2f * JumpPlatformBoost * -controller.Parameters.Gravity ));
	}
}
