using UnityEngine;
using System.Collections;

/// <summary>
/// Bu s�n�f� merdivenlerinize ekler, b�ylece bir Karakter onlara t�rmanabilir.
/// </summary>
public class Ladder : MonoBehaviour 
{
	/// merdivenin tepesindeki platform - bu bir zemin platformu olabilir
	public GameObject ladderPlatform;

	/// <summary>
	/// Merdivene bir �ey �arpt���nda tetiklendi
	/// </summary>
	/// <param name="collider">Merdivene �arpan bir �ey.</param>
	public void OnTriggerEnter2D(Collider2D collider)
	{
		// merdivenle �arp��an nesnenin asl�nda bir corgi denetleyicisi ve bir karakter oldu�unu kontrol ediyoruz
		CharacterBehavior character = collider.GetComponent<CharacterBehavior>();
		if (character==null)
			return;		
		CorgiController controller = collider.GetComponent<CorgiController>();
		if (controller==null)
			return;		
	}

	/// <summary>
	/// Merdivende bir �ey kald���nda tetiklendi
	/// </summary>
	/// <param name="collider">Merdivene �arpan bir �ey.</param>
	public void OnTriggerStay2D(Collider2D collider)
	{
		// merdivenle �arp��an nesnenin asl�nda bir corgi denetleyicisi ve bir karakter oldu�unu kontrol ediyoruz
		CharacterBehavior character = collider.GetComponent<CharacterBehavior>();
		if (character==null)
			return;		
		CorgiController controller = collider.GetComponent<CorgiController>();
		if (controller==null)
			return;

		// H�l� buradaysak bu bir karakter, �arp��ma durumunu ona g�re ayarl�yoruz
		character.BehaviorState.LadderColliding=true;
		// karakter bir merdivene t�rman�yorsa, merdiveni ortalar�z
		if (character.BehaviorState.LadderClimbing)
		{			
			controller.transform.position=new Vector2(transform.position.x,controller.transform.position.y);
		}

		// karakterin ayaklar� merdiven platformunun �zerindeyse, onu merdivenden ��kar�r�z.
		if (ladderPlatform.transform.position.y < controller.transform.position.y-(controller.transform.localScale.y/2)-0.1f)
		{
			character.BehaviorState.LadderClimbing=false;
			character.BehaviorState.CanMoveFreely=true;
			character.BehaviorState.LadderClimbingSpeed=0;			
		}
		
	}

	/// <summary>
	/// Merdivende bir �ey kald���nda tetiklendi
	/// </summary>
	/// <param name="collider">Merdivene �arpan bir �ey.</param>
	public void OnTriggerExit2D(Collider2D collider)
	{
		//merdivenle �arp��an nesnenin asl�nda bir corgi denetleyicisi ve bir karakter oldu�unu kontrol ediyoruz
		CharacterBehavior character = collider.GetComponent<CharacterBehavior>();
		if (character==null)
			return;		
		CorgiController controller = collider.GetComponent<CorgiController>();
		if (controller==null)
			return;

		// karakter art�k merdivenle �arp��mad���nda, merdivenle ilgili �e�itli durumlar�n� s�f�rl�yoruz
		character.BehaviorState.LadderColliding=false;
		character.BehaviorState.LadderClimbing=false;
		character.BehaviorState.CanMoveFreely=true;	
	}
}
