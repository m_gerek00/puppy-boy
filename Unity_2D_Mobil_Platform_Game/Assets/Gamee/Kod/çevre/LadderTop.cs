using UnityEngine;
using System.Collections;
/// <summary>
/// Karakterin merdivenin tepesine ne zaman �arpaca��n� bildi�inden emin olmak i�in bu bile�eni merdivenin �st�ne ekleyin.
/// </summary>
public class LadderTop : MonoBehaviour 
{

	/// <summary>
	/// Ba�ka bir �arp��t�r�c� (genellikle oyuncu) bu bile�enle �arp���rken tetiklenir.
	/// </summary>
	/// <param name="collider">di�er �arp��t�r�c�.</param>
	public void OnTriggerStay2D(Collider2D collider)
	{
		// di�er �arp��t�r�c� bir Karakter Behavior de�ilse, hi�bir �ey yapmay�z
		CharacterBehavior character = collider.GetComponent<CharacterBehavior>();
		if (character==null)
			return;
		// di�er �arp��t�r�c� bir Corgiontroller de�ilse, hi�bir �ey yapmay�z
		CorgiController controller = collider.GetComponent<CorgiController>();
		if (controller==null)
			return;
		// karakter merdivenin tepesiyle �arp���rken, ilgili durumu True olarak ayarl�yoruz.
		character.BehaviorState.LadderTopColliding=true;		
	}
	
	public void OnTriggerExit2D(Collider2D collider)
	{
		// di�er �arp��t�r�c� bir Karakter Behavior de�ilse, hi�bir �ey yapmay�z
		CharacterBehavior character = collider.GetComponent<CharacterBehavior>();
		if (character==null)
			return;
		// di�er �arp��t�r�c� bir Corgiontroller de�ilse, hi�bir �ey yapmay�z	
		CorgiController controller = collider.GetComponent<CorgiController>();
		if (controller==null)
			return;
		// karakter art�k merdivenin tepesiyle �arp��m�yor, kar��l�k gelen durumu yanl�� olarak ayarl�yoruz.
		character.BehaviorState.LadderTopColliding=false;
		
		
	}
}
