﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Zaman içinde yavaşça dans etmesini sağlamak için bu komut dosyasını bir ağaca ekleyin
/// </summary>
public class BackgroundTree : MonoBehaviour 
{

	/// Yeni bir hedef ölçeğin belirlendiği hız (saniye cinsinden). Daha büyük ölçek Hız, daha yavaş hareket anlamına gelir.
	public float scaleSpeed = 0.5f;
	/// Dönüşüm ve yeni hedef ölçek arasındaki maksimum mesafe
	public float scaleDistance = 0.01f;
	/// Dönüş hızı (saniye cinsinden). Daha büyük dönüş hızı, daha hızlı hareket anlamına gelir.
	public float rotationSpeed = 1f;
	/// Dönme genliği (derece cinsinden).
	public float rotationAmplitude = 3f;
	
	private Vector3 _scaleTarget;
	private Quaternion _rotationTarget;	
	private float _accumulator = 0.0f;


	/// <summary>
	/// Hedefleri başlatın
	/// </summary>
	void Start () 
	{
		_scaleTarget = WiggleScale( );
		_rotationTarget = WiggleRotate();	
	}

	/// <summary>
	/// Her karede nesneyi dans ettiririz
	/// </summary>
	void Update () 
	{
		// Her ölçek Hızı, yeni bir ölçek hedefi belirlenir.
		_accumulator += Time.deltaTime;
		if(_accumulator >= scaleSpeed)
		{
			_scaleTarget = WiggleScale();			
			_accumulator -= scaleSpeed;
		}

		// yerel ölçek hedef ölçeğe doğru çevrilir	
		var norm = Time.deltaTime/scaleSpeed;		
		Vector3 newLocalScale=Vector3.Lerp(transform.localScale, _scaleTarget, norm);		
		transform.localScale = newLocalScale;

		// dönüştürme dönüşü hedef dönüşe doğru döndürülür
		var normRotation = Time.deltaTime*rotationSpeed;
		transform.rotation = Quaternion.RotateTowards( transform.rotation, _rotationTarget , normRotation );
		if(transform.rotation == _rotationTarget)
		{			
			_rotationTarget = WiggleRotate();
		}
		
	}

	/// <summary>
	/// Nesnenin ölçeğini kıpırdatır
	/// </summary>
	/// <returns>Nesnenin yeni ölçeği.</returns>
	private Vector3 WiggleScale()
	{
		// Yeni bir ölçek belirler (yalnızca x ve y ekseninde)
		return new Vector3((1 + Random.Range(-scaleDistance,scaleDistance)),(1 + Random.Range(-scaleDistance,scaleDistance)),1);
	}

	/// <summary>
	/// Nesnenin ölçeğini kıpırdatır
	/// </summary>
	/// <returns>Nesnenin yeni ölçeği.</returns>
	private Quaternion WiggleRotate()
	{
		// Yeni bir açı belirler (yalnızca z ekseninde)
		return Quaternion.Euler(0f, 0f, Random.Range(-rotationAmplitude,rotationAmplitude));
	}
	
}
