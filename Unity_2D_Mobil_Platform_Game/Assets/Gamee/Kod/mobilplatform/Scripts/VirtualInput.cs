using System.Collections.Generic;
using UnityEngine;


namespace UnitySampleAssets.CrossPlatformInput
{
    public abstract class VirtualInput
    {
        protected Dictionary<string, CrossPlatformInputManager.VirtualAxis> virtualAxes =
            new Dictionary<string, CrossPlatformInputManager.VirtualAxis>();

        // Sanal eksenlerle ilgili ad� saklamak i�in s�zl�k

        protected Dictionary<string, CrossPlatformInputManager.VirtualButton> virtualButtons =
            new Dictionary<string, CrossPlatformInputManager.VirtualButton>();

        protected List<string> alwaysUseVirtual = new List<string>();
        // Her zaman sanal bir eksen veya d��me kullanmak �zere i�aretlenmi� eksen ve d��me adlar�n�n listesi

        public Vector3 virtualMousePosition { get; private set; }


        public void RegisterVirtualAxis(CrossPlatformInputManager.VirtualAxis axis)
        {
            // Bu isimde bir eksenimiz olup olmad���n� kontrol edin ve varsa g�nl�k ve hata
            if (virtualAxes.ContainsKey(axis.name))
            {
                Debug.LogError("There is already a virtual axis named " + axis.name + " registered.");
            }
            else
            {
                // yeni eksenler ekle
                virtualAxes.Add(axis.name, axis);

                // giri� y�neticisi ayar�yla e�le�tirmek istemiyorsak, her zaman sanal kullanmaya geri d�n�n.
                if (!axis.matchWithInputManager)
                {
                    alwaysUseVirtual.Add(axis.name);
                }
            }
        }


        public void RegisterVirtualButton(CrossPlatformInputManager.VirtualButton button)
        {
            // zaten bu isimde bir d��meniz olup olmad���n� kontrol edin ve varsa bir hata kaydedin
            if (virtualButtons.ContainsKey(button.name))
            {
                Debug.LogError("There is already a virtual button named " + button.name + " registered.");
            }
            else
            {
                // herhangi bir yeni d��me ekle
                virtualButtons.Add(button.name, button);

                // giri� y�neticisiyle e�le�tirmek istemiyorsak, her zaman sanal bir eksen kullan�n
                if (!button.matchWithInputManager)
                {
                    alwaysUseVirtual.Add(button.name);
                }
            }
        }


        public void UnRegisterVirtualAxis(string name)
        {
            // Bu isimde bir eksenimiz varsa, onu kay�tl� eksenler s�zl���m�zden kald�r�n
            if (virtualAxes.ContainsKey(name))
            {
                virtualAxes.Remove(name);
            }
        }


        public void UnRegisterVirtualButton(string name)
        {
            // bu isimde bir d��memiz varsa, onu kay�tl� d��meler s�zl���m�zden kald�r�n
            if (virtualButtons.ContainsKey(name))
            {
                virtualButtons.Remove(name);
            }
        }


        // Aksi takdirde bo�sa, adland�r�lm�� bir sanal eksene bir ba�vuru d�nd�r�r
        public CrossPlatformInputManager.VirtualAxis VirtualAxisReference(string name)
        {
            return virtualAxes.ContainsKey(name) ? virtualAxes[name] : null;
        }


        public void SetVirtualMousePositionX(float f)
        {
            virtualMousePosition = new Vector3(f, virtualMousePosition.y, virtualMousePosition.z);
        }


        public void SetVirtualMousePositionY(float f)
        {
            virtualMousePosition = new Vector3(virtualMousePosition.x, f, virtualMousePosition.z);
        }


        public void SetVirtualMousePositionZ(float f)
        {
            virtualMousePosition = new Vector3(virtualMousePosition.x, virtualMousePosition.y, f);
        }


        public abstract float GetAxis(string name, bool raw);

        public abstract bool GetButton(string name);
        public abstract bool GetButtonDown(string name);
        public abstract bool GetButtonUp(string name);

        public abstract void SetButtonDown(string name);
        public abstract void SetButtonUp(string name);
        public abstract void SetAxisPositive(string name);
        public abstract void SetAxisNegative(string name);
        public abstract void SetAxisZero(string name);
        public abstract void SetAxis(string name, float value);
        public abstract Vector3 MousePosition();
    }
}