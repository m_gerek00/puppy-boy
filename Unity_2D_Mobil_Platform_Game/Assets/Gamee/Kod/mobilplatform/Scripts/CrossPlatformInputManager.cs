using System;
using UnityEngine;
using UnitySampleAssets.CrossPlatformInput.PlatformSpecific;


namespace UnitySampleAssets.CrossPlatformInput
{
    public static class CrossPlatformInputManager
    {
        private static VirtualInput virtualInput;


        static CrossPlatformInputManager()
        {
#if MOBILE_INPUT
            virtualInput = new MobileInput ();
#else
            virtualInput = new StandaloneInput();
#endif
        }


        public static void RegisterVirtualAxis(VirtualAxis axis)
        {
            virtualInput.RegisterVirtualAxis(axis);
        }


        public static void RegisterVirtualButton(VirtualButton button)
        {
            virtualInput.RegisterVirtualButton(button);
        }


        public static void UnRegisterVirtualAxis(string _name)
        {
            if (_name == null)
            {
                throw new ArgumentNullException("_name");
            }
            virtualInput.UnRegisterVirtualAxis(_name);
        }


        public static void UnRegisterVirtualButton(string name)
        {
            virtualInput.UnRegisterVirtualButton(name);
        }


        // Aksi takdirde bo�sa, adland�r�lm�� bir sanal eksene bir ba�vuru d�nd�r�r
        public static VirtualAxis VirtualAxisReference(string name)
        {
            return virtualInput.VirtualAxisReference(name);
        }


        // verilen isim i�in platforma uygun ekseni d�nd�r�r
        public static float GetAxis(string name)
        {
            return GetAxis(name, false);
        }


        public static float GetAxisRaw(string name)
        {
            return GetAxis(name, true);
        }


        // private her iki ekseni de i�ler (ham ve ham de�il)
        private static float GetAxis(string name, bool raw)
        {
            return virtualInput.GetAxis(name, raw);
        }


        // -- Button handling --
        public static bool GetButton(string name)
        {
            return virtualInput.GetButton(name);
        }


        public static bool GetButtonDown(string name)
        {
            return virtualInput.GetButtonDown(name);
        }


        public static bool GetButtonUp(string name)
        {
            return virtualInput.GetButtonUp(name);
        }


        public static void SetButtonDown(string name)
        {
            virtualInput.SetButtonDown(name);
        }


        public static void SetButtonUp(string name)
        {
            virtualInput.SetButtonUp(name);
        }


        public static void SetAxisPositive(string name)
        {
            virtualInput.SetAxisPositive(name);
        }


        public static void SetAxisNegative(string name)
        {
            virtualInput.SetAxisNegative(name);
        }


        public static void SetAxisZero(string name)
        {
            virtualInput.SetAxisZero(name);
        }


        public static void SetAxis(string name, float value)
        {
            virtualInput.SetAxis(name, value);
        }


        public static Vector3 mousePosition
        {
            get { return virtualInput.MousePosition(); }
        }


        public static void SetVirtualMousePositionX(float f)
        {
            virtualInput.SetVirtualMousePositionX(f);
        }


        public static void SetVirtualMousePositionY(float f)
        {
            virtualInput.SetVirtualMousePositionY(f);
        }


        public static void SetVirtualMousePositionZ(float f)
        {
            virtualInput.SetVirtualMousePositionZ(f);
        }


        // sanal eksen ve d��me s�n�flar� - mobil giri� i�in ge�erlidir
        // �stenilen uygulamaya ba�l� olarak kumanda kollar�na, e�imlere, cayroya vb. Dokunmak i�in e�le�tirilebilir.
        // Di�er giri� cihazlar� taraf�ndan da uygulanabilir - kinect, elektronik sens�rler vb.
        public class VirtualAxis
        {
            public string name { get; private set; }
            private float m_Value;
            public bool matchWithInputManager { get; private set; }


            public VirtualAxis(string name) : this(name, true)
            {
            }


            public VirtualAxis(string name, bool matchToInputSettings)
            {
                this.name = name;
                matchWithInputManager = matchToInputSettings;
                RegisterVirtualAxis(this);
            }


            // �apraz platform giri� sisteminden bir ekseni kald�r�r
            public void Remove()
            {
                UnRegisterVirtualAxis(name);
            }


            // bir denetleyici oyun nesnesi (�r. sanal bir kontrol �ubu�u) bu s�n�f� g�ncellemelidir
            public void Update(float value)
            {
                m_Value = value;
            }


            public float GetValue
            {
                get { return m_Value; }
            }


            public float GetValueRaw
            {
                get { return m_Value; }
            }
        }

        // bir denetleyici oyun nesnesi (�r. sanal bir GUI d��mesi),
        // Bu s�n�f�n 'preslenmi�' i�levi. Di�er nesneler daha sonra
        // Bu d��menin Get / Down / Up durumu.
        public class VirtualButton
        {
            public string name { get; private set; }
            private int lastPressedFrame = -5;
            private int releasedFrame = -5;
            private bool pressed;
            public bool matchWithInputManager { get; private set; }


            public VirtualButton(string name) : this(name, true)
            {
            }


            public VirtualButton(string name, bool matchToInputSettings)
            {
                this.name = name;
                matchWithInputManager = matchToInputSettings;
              //  RegisterVirtualButton(this);
            }


            // Bir oyun kumandas� nesnesi, d��meye bas�ld���nda bu i�levi �a��rmal�d�r
            public void Pressed()
            {
                if (pressed)
                {
                    return;
                }
                pressed = true;
                lastPressedFrame = Time.frameCount;
            }


            // Bir oyun kumandas� nesnesi, d��me b�rak�ld���nda bu i�levi �a��rmal�d�r
            public void Released()
            {
                pressed = false;
                releasedFrame = Time.frameCount;
            }


            // oyun nesnesi, d��me yok edildi�inde veya devre d��� b�rak�ld���nda Kald�r'� �a��rmal�d�r
            public void Remove()
            {
                UnRegisterVirtualButton(name);
            }


            // bunlar, �apraz platform giri� sistemi arac�l���yla okunabilen buton durumlar�d�r
            public bool GetButton
            {
                get { return pressed; }
            }


            public bool GetButtonDown
            {
                get
                {
                    return lastPressedFrame - Time.frameCount == 0;
                }
            }


            public bool GetButtonUp
            {
                get
                {
                    return (releasedFrame == Time.frameCount - 0);
                }
            }
        }
    }
}