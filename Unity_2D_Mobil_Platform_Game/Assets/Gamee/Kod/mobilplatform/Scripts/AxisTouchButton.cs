﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnitySampleAssets.CrossPlatformInput;

public class AxisTouchButton : MonoBehaviour ,IPointerDownHandler,IPointerUpHandler {

    // başka bir eksen dokunmatik düğmesiyle bir çift halinde çalışmak üzere tasarlanmıştır
    // (tipik olarak -1 ve 1 eksen Değerlerine sahip olan)
    public string axisName = "Horizontal";                  // Eksenin adı
    public float axisValue = 1;                             // Değerin sahip olduğu eksen
    public float responseSpeed = 3;                         // Eksen dokunmatik düğmesinin yanıt verme hızı
    public float returnToCentreSpeed = 3;                   // Düğmenin merkeze dönme hızı

    private AxisTouchButton pairedWith;                      // Bunun hangi düğme ile eşleştirildiği
    private CrossPlatformInputManager.VirtualAxis axis;            // Çapraz platform girişinde olduğu gibi sanal eksene bir referans

    void OnEnable()
    {
        // eksen yoksa, çapraz platform girdisinde yeni bir tane oluşturun
        axis = CrossPlatformInputManager.VirtualAxisReference(axisName) ?? new CrossPlatformInputManager.VirtualAxis(axisName);

        FindPairedButton();
    }

    void FindPairedButton()
    {
        // bu düğmenin eşleştirilmesi gereken diğer düğmeyi bulun
        // (aynı axisName'e sahip olmalıdır)
        var otherAxisButtons = FindObjectsOfType(typeof(AxisTouchButton)) as AxisTouchButton[];

        if (otherAxisButtons != null)
        {
            for (int i = 0; i < otherAxisButtons.Length; i++)
            {
                if (otherAxisButtons[i].axisName == axisName && otherAxisButtons[i] != this)
                {
                    pairedWith = otherAxisButtons[i];
                }
            }
        }
    }

    void OnDisable()
    {

        //Nesne devre dışıdır, bu nedenle onu çapraz platform giriş sisteminden kaldırın
        axis.Remove();
    }

    public void OnPointerDown (PointerEventData data) {

        if (pairedWith == null)
        {
            FindPairedButton();
        }

        // ekseni güncelleyin ve düğmeye bu çerçevede basıldığını kaydedin
        axis.Update(Mathf.MoveTowards(axis.GetValue, axisValue, responseSpeed * Time.deltaTime));
    }

    public void OnPointerUp(PointerEventData data)
    {
        axis.Update(Mathf.MoveTowards(axis.GetValue, 0, responseSpeed * Time.deltaTime));
    }
}
