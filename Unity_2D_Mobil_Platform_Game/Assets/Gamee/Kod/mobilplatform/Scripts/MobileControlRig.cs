﻿using UnityEngine;


namespace UnitySampleAssets.CrossPlatformInput
{
	#if UNITY_EDITOR
	using UnityEditor;
	[ExecuteInEditMode]
	#endif
	public class MobileControlRig : MonoBehaviour
	{

		// bu komut dosyası bir kontrol donanımının alt nesnelerini etkinleştirir veya devre dışı bırakır
		// USE_MOBILE_INPUT tanımının bildirilip bildirilmediğine bağlı olarak.

		// Bu tanım, aşağıdakilere dahil olan bir menü öğesi tarafından ayarlanır veya kaldırılır:

#if !UNITY_EDITOR
		void OnEnable()
		{
			CheckEnableControlRig();
		}
#endif

#if UNITY_EDITOR

		private void OnEnable()
		{
			EditorUserBuildSettings.activeBuildTargetChanged += Update;
			EditorApplication.update += Update;
		}
		
		private void OnDisable()
		{
			EditorUserBuildSettings.activeBuildTargetChanged -= Update;
			EditorApplication.update -= Update;
		}
		
		private void Update()
		{
			CheckEnableControlRig();
			
		}
		#endif
		
		private void CheckEnableControlRig()
		{
			#if MOBILE_INPUT
			EnableControlRig(true);
			#else
			EnableControlRig(false);
			#endif
			
		}
		
		private void EnableControlRig(bool enabled)
		{
			foreach (Transform t in transform)
			{
				t.gameObject.SetActive(enabled);
			}
		}
	}
}

