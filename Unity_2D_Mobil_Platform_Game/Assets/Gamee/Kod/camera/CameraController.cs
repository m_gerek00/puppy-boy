using UnityEngine;
using System.Collections;
/// <summary>
/// Corgi Motorunun Kamera Kontrol�r�. Kamera hareketini, titremeyi, oyuncu takibini y�netir.
/// </summary>
public class CameraController : MonoBehaviour 
{
	/// Kameran�n Oyuncudan ne kadar uzakta olmas� gerekti�i	
	public float AheadFactor = 3;
	/// Kameran�n Oyuncuya ne kadar h�zl� geri d�nd���
	public float ReturnSpeed = 0.5f;
	/// Kamera threshold
	public float Threshold = 0.1f;
	/// Kamera ne kadar h�zl� yava�l�yor
	public float SlowFactor = 0.3f;
	/// Kamera oyuncuyu takip ederse do�rudur
	public bool FollowsPlayer{get;set;}
	
	// De�i�ikenler
	
	private Transform _target;
	private LevelLimits _levelBounds;
	
	private float xMin;
	private float xMax;
	private float yMin;
	private float yMax;	
	
	private float offsetZ;
	private Vector3 lastTargetPosition;
	private Vector3 currentVelocity;
	private Vector3 lookAheadPos;
	
	private float shakeIntensity;
	private float shakeDecay;
	private float shakeDuration;

	/// <summary>
	/// Ba�latma
	/// </summary>
	void Start ()
	{
		// Kameray� oyuncuyu takip ettiriyoruz
		FollowsPlayer = true;

		// oynat�c� ve seviye s�n�rlar� ba�latma
		_target = GameManager.Instance.Player.transform;
		_levelBounds = GameObject.FindGameObjectWithTag("LevelBounds").GetComponent<LevelLimits>();

		// hedefin son konumunu saklar�z
		lastTargetPosition = _target.position;
		offsetZ = (transform.position - _target.position).z;
		transform.parent = null;

		// kamera boyutu hesaplamas� (orthographicSize, kameran�n g�rd��� y�ksekli�in yar�s�d�r.
		float cameraHeight = Camera.main.orthographicSize * 2f;		
		float cameraWidth = cameraHeight * Camera.main.aspect;

		// kameray� seviyeye kilitlemek i�in seviye s�n�r� koordinatlar�n� al�yoruz
		xMin = _levelBounds.LeftLimit+(cameraWidth/2);
		xMax = _levelBounds.RightLimit-(cameraWidth/2); 
		yMin = _levelBounds.BottomLimit+(cameraHeight/2); 
		yMax = _levelBounds.TopLimit-(cameraHeight/2);		
	}

	/// <summary>
	/// Her kare, gerekirse kameray� hareket ettiririz
	/// </summary>
	void LateUpdate () 
	{
		// kameran�n oyuncuyu takip etmemesi gerekiyorsa hi�bir �ey yapmay�z
		if (!FollowsPlayer)
			return;

		// oyuncu son g�ncellemeden bu yana hareket etmi�se
		float xMoveDelta = (_target.position - lastTargetPosition).x;
		
		bool updateLookAheadTarget = Mathf.Abs(xMoveDelta) > Threshold;
		
		if (updateLookAheadTarget) 
		{
			lookAheadPos = AheadFactor * Vector3.right * Mathf.Sign(xMoveDelta);
		} 
		else 
		{
			lookAheadPos = Vector3.MoveTowards(lookAheadPos, Vector3.zero, Time.deltaTime * ReturnSpeed);	
		}
		
		Vector3 aheadTargetPos = _target.position + lookAheadPos + Vector3.forward * offsetZ;
		Vector3 newPos = Vector3.SmoothDamp(transform.position, aheadTargetPos, ref currentVelocity, SlowFactor);
		
		
		Vector3 shakeFactorPosition = new Vector3(0,0,0);

		// ShakeDuration hala �al���yorsa.
		if (shakeDuration>0)
		{
			shakeFactorPosition= Random.insideUnitSphere * shakeIntensity * shakeDuration;
			shakeDuration-=shakeDecay*Time.deltaTime ;
		}
		
		newPos = newPos+shakeFactorPosition;

		// Level s�n�rlar�
		float posX = Mathf.Clamp(newPos.x, xMin, xMax);
		float posY = Mathf.Clamp(newPos.y, yMin, yMax);
		float posZ = newPos.z;

		//yeni pozisyon olu�turun - belirlenen s�n�rlar i�inde
		newPos = new Vector3(posX, posY, posZ);
		
		transform.position=newPos;
		
		lastTargetPosition = _target.position;		
	}

	/// <summary>
	/// Yo�unluk, s�re ve bozulma i�in bir Vector3'e ge�erek kameray� sallamak i�in bu y�ntemi kullan�n.
	/// </summary>
	/// <param name="shakeParameters">Shake parametreleri: yo�unluk, s�re ve azalma.</param>
	public void Shake(Vector3 shakeParameters)
	{
		shakeIntensity = shakeParameters.x;
		shakeDuration=shakeParameters.y;
		shakeDecay=shakeParameters.z;
	}
}