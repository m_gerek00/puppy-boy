﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
/// <summary>
/// Paralaks içinde hareket etmesini sağlamak için bunu bir GameObject'e ekleyin
/// </summary>
public class ParallaxLayer : MonoBehaviour 
{
	/// katmanın yatay hızı
	public float speedX;
	/// katmanın dikey hızı
	public float speedY;
	/// katmanın kamera ile aynı yönde hareket edip etmediğini tanımlar
	public bool moveInOppositeDirection;

	private Transform cameraTransform;
	private Vector3 previousCameraPosition;
	private bool previousMoveParallax;
	private ParallaxSetup options;

	/// <summary>
	/// Başlatma
	/// </summary>
	void OnEnable() 
	{
		GameObject gameCamera = GameObject.Find("Main Camera");
		options = gameCamera.GetComponent<ParallaxSetup>();
		cameraTransform = gameCamera.transform;
		previousCameraPosition = cameraTransform.position;
	}

	/// <summary>
	/// Her karede paralaks katmanını kameranın konumuna göre hareket ettiriyoruz
	/// </summary>
	void Update () 
	{
		if(options.moveParallax && !previousMoveParallax)
			previousCameraPosition = cameraTransform.position;

		previousMoveParallax = options.moveParallax;

		if(!Application.isPlaying && !options.moveParallax)
			return;

		Vector3 distance = cameraTransform.position - previousCameraPosition;
		float direction = (moveInOppositeDirection) ? -1f : 1f;
		transform.position += Vector3.Scale(distance, new Vector3(speedX, speedY)) * direction;

		previousCameraPosition = cameraTransform.position;
	}
}
