﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Paralaks katmanlarını desteklemesi için bu sınıfı bir kameraya ekleyin
/// </summary>
public class ParallaxSetup : MonoBehaviour 
{
	
	public bool moveParallax;

	[SerializeField]
	[HideInInspector]
	private Vector3 storedPosition;

	public void SavePosition() 
	{
		storedPosition = transform.position;
	}

	public void RestorePosition() 
	{
		transform.position = storedPosition;
	}
}