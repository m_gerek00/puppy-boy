using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
/// <summary>
/// Bu s�n�f� bir tetikleyiciye ekleyin ve oyuncunuzu bir sonraki seviyeye g�nderecektir.
/// </summary>
public class FinishLevel : MonoBehaviour 
{
	public string LevelName;


	/// <param name="collider">teti�imizle �arp��an bir �arp��t�r�c�.</param>
	public void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.GetComponent<CharacterBehavior>() == null)
			return;

		LevelManager.Instance.GotoLevel(LevelName);
	}
}
