﻿using UnityEngine;
using System.Collections;

public class SoundManager : PersistentSingleton<SoundManager>
{	
	public bool MusicOn=true;
	public bool SfxOn=true;
	[Range(0,1)]
	public float MusicVolume=0.3f;
	[Range(0,1)]
	public float SfxVolume=1f;
		
	private AudioSource _backgroundMusic;	
		
	/// <param name="Clip">ses klip.</param>
	public void PlayBackgroundMusic(AudioSource Music)
	{
		if (!MusicOn)
			return;
		if (_backgroundMusic!=null)
			_backgroundMusic.Stop();
		_backgroundMusic=Music;
		_backgroundMusic.volume=MusicVolume;
		_backgroundMusic.loop=true;
		_backgroundMusic.Play();		
	}

	/// <returns>An audiosource</returns>
	/// <param name="Sfx">Oynatmak istediğiniz ses klibi.</param>
	/// <param name="Location">Sesin yeri.</param>
	/// <param name="Volume">Sesin seviyesi.</param>
	public AudioSource PlaySound(AudioClip Sfx, Vector3 Location)
	{
		GameObject temporaryAudioHost = new GameObject("TempAudio");
		temporaryAudioHost.transform.position = Location;
		AudioSource audioSource = temporaryAudioHost.AddComponent<AudioSource>() as AudioSource; 
		audioSource.clip = Sfx; 
		audioSource.volume = SfxVolume;
		audioSource.Play(); 
		Destroy(temporaryAudioHost, Sfx.length);
		return audioSource;
	}
}
