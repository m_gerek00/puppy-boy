using UnityEngine;
using System.Collections;
using UnitySampleAssets.CrossPlatformInput;

public class InputManager : PersistentSingleton<InputManager>
{
		
	private static CharacterBehavior _player;
	
	void Start()
	{
		if (GameManager.Instance.Player!=null)
			_player = GameManager.Instance.Player;	
	}
	
	void Update()
	{		
		if (_player == null) 
		{
			if (GameManager.Instance.Player!=null)
			{
				if (GameManager.Instance.Player.GetComponent<CharacterBehavior> () != null)
					_player = GameManager.Instance.Player;
			}
			else
				return;
		}
		
		if ( CrossPlatformInputManager.GetButtonDown("Pause") )
			GameManager.Instance.Pause();
			
		if (GameManager.Instance.Paused)
			return;
			
		_player.SetHorizontalMove(CrossPlatformInputManager.GetAxis ("Horizontal"));
		_player.SetVerticalMove(CrossPlatformInputManager.GetAxis ("Vertical"));
		
		if ((CrossPlatformInputManager.GetButtonDown("Run")||CrossPlatformInputManager.GetButton("Run")) )
			_player.RunStart();		
		
		if (CrossPlatformInputManager.GetButtonUp("Run"))
			_player.RunStop();		
				
		if (CrossPlatformInputManager.GetButtonDown("Jump"))
			_player.JumpStart();
						
		if (CrossPlatformInputManager.GetButtonUp("Jump"))
			_player.JumpStop();
					
		if ( CrossPlatformInputManager.GetButtonDown("Dash") )
			_player.Dash();
			
				
		if (_player.GetComponent<CharacterMelee>() != null) 
		{		
			if ( CrossPlatformInputManager.GetButtonDown("Melee")  )
				_player.GetComponent<CharacterMelee>().Melee();
		}		
		
		if (_player.GetComponent<CharacterShoot>() != null) 
		{
			_player.GetComponent<CharacterShoot>().SetHorizontalMove(CrossPlatformInputManager.GetAxis ("Horizontal"));
			_player.GetComponent<CharacterShoot>().SetVerticalMove(CrossPlatformInputManager.GetAxis ("Vertical"));

			if (CrossPlatformInputManager.GetButtonDown("Fire"))
				_player.GetComponent<CharacterShoot>().ShootOnce();			
			
			if (CrossPlatformInputManager.GetButton("Fire")) 
				_player.GetComponent<CharacterShoot>().ShootStart();
			
			if (CrossPlatformInputManager.GetButtonUp("Fire"))
				_player.GetComponent<CharacterShoot>().ShootStop();

		}

		if (_player.GetComponent<CharacterJetpack>()!=null)
		{
			if ((CrossPlatformInputManager.GetButtonDown("Jetpack")||CrossPlatformInputManager.GetButton("Jetpack")) )
				_player.GetComponent<CharacterJetpack>().JetpackStart();
			
			if (CrossPlatformInputManager.GetButtonUp("Jetpack"))
				_player.GetComponent<CharacterJetpack>().JetpackStop();
		}
	}	
}
