﻿using UnityEngine;
using System.Collections;

public class GameManager : PersistentSingleton<GameManager>
{		
	public int Points { get; private set; }
	public float TimeScale { get; private set; }
	public bool Paused { get; set; } 
	public CharacterBehavior Player { get; set; }

	private float _savedTimeScale;
				
	
	public void Reset()
	{
		Points = 0;
		TimeScale = 1f;
		Paused = false;
		GUIManager.Instance.RefreshPoints ();
	}

	/// <summary>
	/// Mevcut oyun puanlarına parametrelerdeki puanları ekler.
	/// </summary>
	/// <param name="pointsToAdd">Nokta Ekle.</param>
	public void AddPoints(int pointsToAdd)
	{
		Points += pointsToAdd;
		GUIManager.Instance.RefreshPoints ();
	}
	
	/// <param name="points">Nokta.</param>
	public void SetPoints(int points)
	{
		Points = points;
		GUIManager.Instance.RefreshPoints ();
	}
	
	/// <param name="newTimeScale">New time scale.</param>
	public void SetTimeScale(float newTimeScale)
	{
		_savedTimeScale = Time.timeScale;
		Time.timeScale = newTimeScale;
	}
	
	public void ResetTimeScale()
	{
		Time.timeScale = _savedTimeScale;
	}
	
	public void Pause()
	{
		// eğer zaman zaten durmamışsa	
		if (Time.timeScale>0.0f)
		{
			Instance.SetTimeScale(0.0f);
			Instance.Paused=true;
			GUIManager.Instance.SetPause(true);
		}
		else
		{
			Instance.ResetTimeScale();	
			Instance.Paused=false;
			GUIManager.Instance.SetPause(false);	
		}		
	}	
}
