﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Oyuncu tetiğe ulaştığında seviyenin yeniden başlamasını sağlamak için bu sınıfı bir tetikleyiciye ekleyin
/// </summary>
public class LevelRestarter : MonoBehaviour 
{	
	void OnTriggerEnter2D (Collider2D collider)
	{
		if(collider.tag == "Player")
			Application.LoadLevel(Application.loadedLevelName);
	}
}