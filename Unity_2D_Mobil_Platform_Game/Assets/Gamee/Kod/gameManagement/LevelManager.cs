using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
	public static LevelManager Instance { get; private set; }		
	public CharacterBehavior PlayerPrefab ;
	public CameraController Camera { get; private set; }
	public TimeSpan RunningTime { get { return DateTime.UtcNow - _started ;}}
	public CheckPoint DebugSpawn;
	public float IntroFadeDuration=1f;
	public float OutroFadeDuration=1f;
	
	
	private CharacterBehavior _player;
	private List<CheckPoint> _checkpoints;
	private int _currentCheckPointIndex;
	private DateTime _started;
	private int _savedPoints;
	
	public void Awake()
	{
		Instance=this;
		_player = (CharacterBehavior)Instantiate(PlayerPrefab, new Vector3(0, 0, 0), Quaternion.identity);
		GameManager.Instance.Player=_player;
	}
	
	public void Start()
	{
		_savedPoints=GameManager.Instance.Points;
		_checkpoints = FindObjectsOfType<CheckPoint>().OrderBy(t => t.transform.position.x).ToList();
		_currentCheckPointIndex = _checkpoints.Count > 0 ? 0 : -1;
		_started = DateTime.UtcNow;

		// kameray� al�yoruz			
		Camera = FindObjectOfType<CameraController>();

		// yumurtlama noktalar�n�n listesini al�yoruz
		var listeners = FindObjectsOfType<MonoBehaviour>().OfType<IPlayerRespawnListener>();
		foreach(var listener in listeners)
		{
			for (var i = _checkpoints.Count - 1; i>=0; i--)
			{
				var distance = ((MonoBehaviour) listener).transform.position.x - _checkpoints[i].transform.position.x;
				if (distance<0)
					continue;
				
				_checkpoints[i].AssignObjectToCheckPoint(listener);
				break;
			}
		}
		
		GUIManager.Instance.SetLevelName(Application.loadedLevelName);
		
		GUIManager.Instance.FaderOn(false,IntroFadeDuration);

		#if UNITY_EDITOR
		if (DebugSpawn!= null)
		{
			DebugSpawn.SpawnPlayer(_player);
		}
		else if (_currentCheckPointIndex != -1)
		{
			_checkpoints[_currentCheckPointIndex].SpawnPlayer(_player);
		}
		#else
		if (_currentCheckPointIndex != -1)
		{			
			_checkpoints[_currentCheckPointIndex].SpawnPlayer(_player);
		}
		#endif		
	}

	public void Update()
	{
		var isAtLastCheckPoint = _currentCheckPointIndex + 1 >= _checkpoints.Count;
		if (isAtLastCheckPoint)
			return;
		
		var distanceToNextCheckPoint = _checkpoints[_currentCheckPointIndex+1].transform.position.x - _player.transform.position.x;
		if (distanceToNextCheckPoint>=0)
			return;
		
		_checkpoints[_currentCheckPointIndex].PlayerLeftCheckPoint();
		
		_currentCheckPointIndex++;
		_checkpoints[_currentCheckPointIndex].PlayerHitCheckPoint();
		
		_savedPoints = GameManager.Instance.Points;
		_started = DateTime.UtcNow;
	}

	/// <param name="levelName">Level ismi.</param>
	public void GotoLevel(string levelName)
	{		
		GUIManager.Instance.FaderOn(true,OutroFadeDuration);
		StartCoroutine(GotoLevelCo(levelName));
	}

	/// <returns>The level co.</returns>
	/// <param name="levelName">Level ismi.</param>
	private IEnumerator GotoLevelCo(string levelName)
	{
		_player.Disable();
		yield return new WaitForSeconds(OutroFadeDuration);
		
		if (string.IsNullOrEmpty(levelName))
			Application.LoadLevel("Ana_Men�");
		else
			Application.LoadLevel(levelName);
		
	}

	public void KillPlayer()
	{
		StartCoroutine(KillPlayerCo());
	}

	/// <returns>The player co.</returns>
	private IEnumerator KillPlayerCo()
	{
		_player.Kill();
		Camera.FollowsPlayer=false;
		yield return new WaitForSeconds(2f);
		
		Camera.FollowsPlayer=true;
		if (_currentCheckPointIndex!=-1)
			_checkpoints[_currentCheckPointIndex].SpawnPlayer(_player);
		
		_started = DateTime.UtcNow;
		GameManager.Instance.SetPoints(_savedPoints);
	}
}

