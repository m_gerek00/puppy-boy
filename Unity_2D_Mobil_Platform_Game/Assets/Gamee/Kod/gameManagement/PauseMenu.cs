﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    public static bool gamepause = false;
    public GameObject pauseMenuUI;
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape)){
            if(gamepause){
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }
    public void Resume(){
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        gamepause = false;

    }
    void Pause(){
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        gamepause = true;
    }
    public void LoadMenu(){
        Time.timeScale =1f;
        Application.LoadLevel(0);

    }
    public void Quit()
    {
        Application.Quit();

    }
}
