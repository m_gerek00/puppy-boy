﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Örneklendiğinde bir arka plan müziği çalması için bu sınıfı bir GameObject'e ekleyin.
/// Dikkat: bir seferde yalnızca bir arka plan müziği çalınacaktır.
/// </summary>
public class BackgroundMusic : PersistentHumbleSingleton<BackgroundMusic>
{
	public AudioClip SoundClip ;
	private AudioSource _source;
	
	void Start () 
	{
		_source = gameObject.AddComponent<AudioSource>() as AudioSource;	
		_source.playOnAwake=false;
		_source.spatialBlend=0;
		_source.rolloffMode = AudioRolloffMode.Logarithmic;
		_source.loop=true;	
	
		_source.clip=SoundClip;
		
		SoundManager.Instance.PlayBackgroundMusic(_source);
	}
}
