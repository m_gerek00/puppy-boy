using UnityEngine;
using System.Collections;
/// <summary>
///Mermilerin davran��lar�n� idare eder
/// </summary>
public abstract class Projectile : MonoBehaviour 
{	
	/// Merminini h�z�
	public float Speed;
	/// merminin �arp��ma maskesi
	public LayerMask CollisionMask;
	/// the projectile's owner
	public GameObject Owner {get; private set; }
	/// merminin ba�lang�� y�n�
	public Vector2 Direction  {get; private set; }
	/// merminin ba�lang�� h�z�
	public Vector2 InitialVelocity  {get; private set; }

	/// <summary>
	/// Belirtilen sahibi, y�n� ve initialVelocity'yi ba�lat�n.
	/// </summary>
	/// <param name="owner">Sahip.</param>
	/// <param name="direction">Y�n�.</param>
	/// <param name="initialVelocity">�lk h�z.</param>
	public void Initialize (GameObject owner, Vector2 direction, Vector2 initialVelocity )
	{
		transform.right=direction;
		Owner=owner;
		Direction=direction;
		InitialVelocity=initialVelocity;
		
		OnInitialized();
	}

	protected virtual void OnInitialized()
	{
		// �u anda hi�bir �ey yapm�yor
	}

	/// <summary>
	/// mermi bir �eyle �arp��t���nda tetiklenir
	/// </summary>
	/// <param name="collider">Collider.</param>
	public virtual void OnTriggerEnter2D(Collider2D collider)
	{
		// vurdu�umuz �arp��t�r�c� do�ru �arp��ma maskesinde de�ilse, hi�bir �ey yapmay�z ve ��k�yoruz
		if ((CollisionMask.value & (1 << collider.gameObject.layer)) == 0)
		{
			OnNotCollideWith(collider);
			return;
		}
		
		var isOwner = collider.gameObject == Owner;
		if (isOwner)
		{
			OnCollideOwner();
			return;
		}
		
		var takeDamage= (CanTakeDamage) collider.GetComponent(typeof(CanTakeDamage));
		if (takeDamage!=null)
		{
			OnCollideTakeDamage(collider,takeDamage);
			return;
		}
		
		OnCollideOther(collider);
	}
	
	protected virtual void OnNotCollideWith(Collider2D collider)
	{
	
	}
	
	protected virtual void OnCollideOwner()
	{
	
	}
	
	protected virtual void OnCollideTakeDamage(Collider2D collider, CanTakeDamage takeDamage)
	{
	
	}
	
	protected virtual void OnCollideOther(Collider2D collider)
	{
	
	}
	
}
