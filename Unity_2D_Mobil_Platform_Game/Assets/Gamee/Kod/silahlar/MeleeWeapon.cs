using UnityEngine;
using System.Collections;
/// <summary>
/// Yak�n d�v�� sald�r�lar�lar� kontrol
/// </summary>
public class MeleeWeapon : MonoBehaviour 
{
	/// yak�n d�v�� sald�r�s� taraf�ndan vurulacak �arp��ma maskesi
	public LayerMask CollisionMask;
	/// verilecek hasar miktar�
	public int Damage;
	/// isabet �zerine somutla�t�rma etkisi
	public GameObject HitEffect;
	/// sald�r�n�n sahibi
	public GameObject Owner;

	/// <summary>
	/// Yak�n d�v�� sald�r�s�yla bir �ey �arp��t���nda tetiklendi
	/// </summary>
	/// <param name="collider">Collider.</param>
	public virtual void OnTriggerEnter2D(Collider2D collider)
	{
		// yak�n d�v�� silah�n�n �arp��t��� �arp��t�r�c� hedeflenen katman maskesinde de�ilse hi�bir �ey yapmay�z
		if ((CollisionMask.value & (1 << collider.gameObject.layer)) == 0)
		{
			return;
		}

		// yak�n d�v�� silah�n�n �arp��t��� �arp��t�r�c� sahibiyse (oyuncu), hi�bir �ey yapmay�z
		var isOwner = collider.gameObject == Owner;
		if (isOwner)
		{
			return;
		}

		// yak�n d�v�� silah�n�n �arp��t��� �arp��t�r�c� hasar alabilirse, ona yak�n d�v�� silah�n�n hasar�n� uygular�z ve bir isabet etkisi ba�lat�r�z
		var takeDamage = (CanTakeDamage) collider.GetComponent(typeof(CanTakeDamage));
		if (takeDamage!=null)
		{
			OnCollideTakeDamage(collider,takeDamage);
			return;
		}
		
		OnCollideOther(collider);
	}


	void OnCollideTakeDamage(Collider2D collider, CanTakeDamage takeDamage)
	{
		Instantiate(HitEffect,collider.transform.position,collider.transform.rotation);
		takeDamage.TakeDamage(Damage,gameObject);
		DisableMeleeWeapon();		
	}
	
	void OnCollideOther(Collider2D collider)
	{
		DisableMeleeWeapon();
	}
		
	void DisableMeleeWeapon()
	{
		// E�er daha uzun s�ren yak�n d�v�� animasyonlar�na sahipseniz, yak�n d�v�� silah�n�n �arp��t�r�c�s�n� bir �eye �arpt�ktan sonra animasyonun sonuna kadar devre d��� b�rakmak isteyebilirsiniz.
	}
}
