﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Silah parametreleri
/// </summary>
public class Weapon : MonoBehaviour 
{
	/// mermi türü silahın vurduğu
	public Projectile Projectile;
	/// ateşleme frekansı
	public float FireRate;
	/// silahın dönme merkezi
	public GameObject GunRotationCenter;
	/// Silah her ateşlendiğinde somutlaştıracak parçacık sistemi
	public ParticleSystem GunFlames;
	/// silahın yaydığı mermiler
	public ParticleSystem GunShells;
	/// ilk mermi ateşleme pozisyonu
	public Transform ProjectileFireLocation;
	// oyuncu ateş ettiğinde çalınacak ses
	public AudioClip GunShootFx;

	void Start()
	{
		SetGunFlamesEmission (false);
		SetGunShellsEmission (false);
	}
	
	public void SetGunFlamesEmission(bool state)
	{
		GunFlames.enableEmission=state;	
	}
	
	public void SetGunShellsEmission(bool state)
	{
		GunShells.enableEmission=state;	
	}
}
