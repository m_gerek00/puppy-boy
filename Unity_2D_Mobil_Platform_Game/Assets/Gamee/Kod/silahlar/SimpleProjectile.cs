using UnityEngine;
using System.Collections;
/// <summary>
/// Basit bir mermi davran���
/// </summary>
public class SimpleProjectile : Projectile, CanTakeDamage
{
	/// merminin verdi�i hasar miktar�
	public int Damage;
	/// mermi yok edildi�inde somutla�t�rma etkisi
	public GameObject DestroyedEffect;
	/// oyuncuya yok edildi�inde verilecek puan miktar�
	public int PointsToGiveToPlayer;
	/// merminin �mr�
	public float TimeToLive;

	/// <summary>
	/// Her karede, merminin �mr�n�n dolup dolmad���n� kontrol ediyoruz.
	/// </summary>
	public void Update () 
	{
		// do�ruysa onu yok ederiz
		if ((TimeToLive -= Time.deltaTime) <= 0)
		{
			DestroyProjectile();
			return;
		}
		// mermiyi hareket ettiririz
		transform.Translate(Direction * ((Mathf.Abs (InitialVelocity.x)+Speed) * Time.deltaTime),Space.World);
	}

	/// <summary>
	/// Mermi hasar ald���nda �a�r�l�r
	/// </summary>
	/// <param name="damage">Damage.</param>
	/// <param name="instigator">K��k�rt�c�.</param>
	public void TakeDamage(int damage, GameObject instigator)
	{
		if (PointsToGiveToPlayer!=0)
		{
			var projectile = instigator.GetComponent<Projectile>();
			if (projectile != null && projectile.Owner.GetComponent<CharacterBehavior>() != null)
			{
				GameManager.Instance.AddPoints(PointsToGiveToPlayer);
			}
		}
		
		DestroyProjectile();
	}

	/// <summary>
	/// Mermi bir �eyle �arp��t���nda tetiklendi
	/// </summary>
	/// <param name="collider">Collider.</param>
	protected override void OnCollideOther(Collider2D collider)
	{
		DestroyProjectile();
	}

	/// <summary>
	/// �arp��ma hasar alma olay�n� y�kseltir.
	/// </summary>
	/// <param name="collider">Collider.</param>
	/// <param name="takeDamage">Take damage.</param>
	protected override void OnCollideTakeDamage(Collider2D collider, CanTakeDamage takeDamage)
	{
		takeDamage.TakeDamage(Damage,gameObject);
		DestroyProjectile();
	}

	/// <summary>
	/// Mermiyi yok eder.
	/// </summary>
	private void DestroyProjectile()
	{
		if (DestroyedEffect!=null)
		{
			Instantiate(DestroyedEffect,transform.position,transform.rotation);
		}
		
		Destroy(gameObject);
	}
}
