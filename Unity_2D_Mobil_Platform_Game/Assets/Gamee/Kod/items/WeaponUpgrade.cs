using UnityEngine;
using System.Collections;
/// <summary>
/// Oyuncunun toplarken silah� de�i�tirmesini sa�lamak i�in bu s�n�f� bir koleksiyon par�as�na ekleyin
/// </summary>
public class WeaponUpgrade : MonoBehaviour, IPlayerRespawnListener
{
	/// topland���nda �rnekleme etkisi
	public GameObject Effect;
	///oyuncunun bu nesneyi toplarken ald��� yeni silah
	public Weapon WeaponToGive;

	/// <param name="collider">Other collider.</param>
	public void OnTriggerEnter2D (Collider2D collider) 
	{
		// 
		if (collider.GetComponent<CharacterBehavior>() == null)
		{
			return;
		}		
		Instantiate(Effect,transform.position,transform.rotation);
		
		collider.GetComponent<CharacterShoot>().ChangeWeapon(WeaponToGive);
		
		gameObject.SetActive(false);
	}
	public void onPlayerRespawnInThisCheckpoint(CheckPoint checkpoint, CharacterBehavior player)
	{
		gameObject.SetActive(true);
	}
}
