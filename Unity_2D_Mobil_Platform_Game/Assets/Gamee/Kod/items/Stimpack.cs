using UnityEngine;
using System.Collections;

/// <summary>
/// Toplayan oyuncuya sa�l�k verir
/// </summary>

public class Stimpack : MonoBehaviour,IPlayerRespawnListener
{
	/// nesne topland���nda �rnekleme etkisi
	public GameObject Effect;
	/// oyuncuya topland���nda verilecek sa�l�k miktar�
	public int HealthToGive;

	/// <param name="collider">Other collider.</param>
	public void OnTriggerEnter2D(Collider2D collider)
	{
		var player = collider.GetComponent<CharacterBehavior>(); 
		if (player == null)
			return;

		// Aksi takdirde, oyuncuya sa�l�k veririz
		player.GiveHealth(HealthToGive,gameObject);
		// isabet etkisini somutla�t�r�yoruz
		Instantiate(Effect,transform.position,transform.rotation);
		gameObject.SetActive(false);
	}

	/// <param name="checkpoint">Checkpoint.</param>
	/// <param name="player">Player.</param>
	public void onPlayerRespawnInThisCheckpoint(CheckPoint checkpoint, CharacterBehavior player)
	{
		gameObject.SetActive(true);
	}
}
