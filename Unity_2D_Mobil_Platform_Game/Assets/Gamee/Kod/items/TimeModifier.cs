using UnityEngine;
using System.Collections;
/// <summary>
/// Bir Karakter taraf�ndan al�nd���nda zaman� de�i�tirmesi i�in bunu bir ��eye ekleyin
/// </summary>
public class TimeModifier : MonoBehaviour, IPlayerRespawnListener
{
	///al�nd���nda �rnekleme etkisi
	public GameObject Effect;
	/// efekt s�rerken uygulanacak zaman h�z�
	public float TimeSpeed = 0.5f;
	/// saniye cinsinden s�re ne kadar s�recek
	public float Duration = 1.0f;

	/// <summary>
	/// TimeModifier ile bir �ey �arp��t���nda tetiklendi
	/// </summary>
	/// <param name="collider">TimeModifier ile �arp��an nesne</param>
	public void OnTriggerEnter2D (Collider2D collider) 
	{
		// di�er �arp��t�r�c� bir Karakter Davran��� de�ilse, ��k�p hi�bir �ey yapmay�z
		if (collider.GetComponent<CharacterBehavior>() == null)
			return;
		// ChangeTime coroutine ba�lat�yoruz
		StartCoroutine(ChangeTime ());

		// TimeModifier'�n konumuna efektin bir �rne�ini ekler
		Instantiate(Effect,transform.position,transform.rotation);
		// sprite ve �arp��t�r�c�y� devre d��� b�rak�yoruz
		gameObject.GetComponent<SpriteRenderer> ().enabled = false;
		gameObject.GetComponent<CircleCollider2D> ().enabled = false;
	}

	/// <summary>
	/// Oyun Y�neticisinden belirli bir s�re i�in zaman �l�e�ini de�i�tirmesini ister.
	/// </summary>
	/// <returns>The time.</returns>
	private IEnumerator ChangeTime()
	{
		GameManager.Instance.SetTimeScale (TimeSpeed);
		GUIManager.Instance.SetTimeSplash (true);
		// ger�ek s�reyi saniye cinsinden elde etmek i�in s�reyi zaman h�z�yla �arp�yoruz
		yield return new WaitForSeconds (Duration*TimeSpeed);
		GameManager.Instance.ResetTimeScale ();
		GUIManager.Instance.SetTimeSplash (false);
		// hareketli grafi�i ve �arp��t�r�c�y� etkinle�tiriyoruz ve nesneyi devre d��� b�rak�yoruz
		gameObject.GetComponent<SpriteRenderer> ().enabled = true;
		gameObject.GetComponent<CircleCollider2D> ().enabled = true;
		gameObject.SetActive(false);
	}

	/// <param name="checkpoint">Checkpoint.</param>
	/// <param name="character">Character.</param>
	public void onPlayerRespawnInThisCheckpoint(CheckPoint checkpoint, CharacterBehavior character)
	{
		gameObject.SetActive(true);
	}
}
