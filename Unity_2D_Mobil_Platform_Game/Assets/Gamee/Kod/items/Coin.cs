using UnityEngine;
using System.Collections;
/// <summary>
/// Coin 
/// </summary>
public class Coin : MonoBehaviour, IPlayerRespawnListener
{

	public GameObject Effect;
	public int PointsToAdd = 10;

	/// <param name="collider">Other.</param>
	public void OnTriggerEnter2D (Collider2D collider) 
	{
		// Madeni parayla �arp��an bir karakter de�ilse Davran��, hi�bir �ey yap�p ��k�yoruz
		if (collider.GetComponent<CharacterBehavior>() == null)
			return;

		// Belirtilen miktarda puan� oyun y�neticisine iletiyoruz
		GameManager.Instance.AddPoints(PointsToAdd);
		// madeni paran�n konumuna efektin bir �rne�ini ekler
		Instantiate(Effect,transform.position,transform.rotation);
		// gameobject'i devre d��� b�rak�yoruz
		gameObject.SetActive(false);
	}
	
	/// <param name="checkpoint">Checkpoint.</param>
	/// <param name="player">Player.</param>
	public void onPlayerRespawnInThisCheckpoint(CheckPoint checkpoint, CharacterBehavior player)
	{
		gameObject.SetActive(true);
	}
}
